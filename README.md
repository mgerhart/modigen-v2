 # Spray 2
Is the successor of the original **[Spray Project](http://eclipselabs.org/p/spray/)**. Compared to the old version, we significantly improved the *Spray* language. We also moved the project to the newest Eclipse and the newest required plugins. Spray runs now on Java 1.7 and is also compatible to 1.8. Furthermore, we cleaned up the project's structure and architecture, reduced the dependency hell between the various plugins, and cleaned up the plugins itself. 

# Project Setup

## Requirements
### Java Development Kit
[JDK 1.7](http://www.oracle.com/technetwork/java/javase/downloads/index.html) (tested with Java 7 Update 60)

### Eclipse
Latest **Eclipse** for Model Driven Software Development (Download: [Xtext project's website](http://www.eclipse.org/Xtext/download.html))

  * Install the latest **Graphiti** plugin
    1. Click *Help => Install New Software*
    1. Select the *Kepler update site*
    1. Search for *Graphiti SDK Plus (Incubation)* and install the package.

  * **Tested Eclipse Version**

![eclipseVersion.png](https://bitbucket.org/repo/7R7eny/images/2934347129-eclipseVersion.png)

  * **Tested Eclipse plugin configuration**
    1. Click *Help => Install New Software*
    1. Click *What is already installed?*

![eclipsePluginConfiguration.png](https://bitbucket.org/repo/7R7eny/images/2633903378-eclipsePluginConfiguration.png)

## Eclipse Workspace Setup
  * Encoding and file delimiter
    1. Open the *Eclipse Preferences* and search for *workspace* 
    1. Select *Text file encoding: UTF-8* and *New text file delimiter: UNIX*

  * Java Compiler (only if you use Java 8)
    1. Open the *Eclipse Preferences*
    1. Open category *Java* and select *Compiler*
    1. Set the *Compiler compliance level* to 1.7
  * Clone GIT repository
    
    ```
    git clone https://bitbucket.org/spray2/spray2.git
    ```

  * Import Spray's plugin projects into your workspace
    1. Spray's plugin project's are stored in the *<cloned repository>/spray_projects/* folder

# Build
To build the Spray project, you have to generate the languages' *model code* and execute its *workflow*, which is described following steps.

## 1. *Styles* Language
### 1. Step: Generate Model code
  1. Open the *org.eclipselabs.spray.styles.mm* project
  1. Open the *Styles.genmodel* from the project's *model* folder
  1. Right click the genmodel's *root element* and select *Generate Model Code*

### 2. Step: Execute Language workflow
  1. Open the *org.eclipselabs.spray.styles.xtext* project
  1. Right click on the *GenerateStyles.mwe2.launch* file and select *Run as => GenerateStyles.mew2*
  1. Check if the workflow finished with


    ```
    INFO  .emf.mwe2.runtime.workflow.Workflow  - Done.
    ```

## 2. *Shapes* Language
### 1. Step: Generate Model code
  1. Open the *org.eclipselabs.spray.shapes.mm* project
  1. Open the *Shapes.genmodel* from the project's *model* folder
  1. Right click the genmodel's *root element* and select *Generate Model Code*

### 2. Step: Execute Language workflow
  1. Open the *org.eclipselabs.spray.shapes.xtext* project
  1. Right click on the *GenerateShapes.mwe2.launch* file and select *Run as => GenerateShapes.mew2*
  1. Check if the workflow finished with


    ```
    INFO  .emf.mwe2.runtime.workflow.Workflow  - Done.
    ```

## 3. *Spray* Language
### 1. Step: Generate Model code
  1. Open the *org.eclipselabs.spray.mm* project
  1. Open the *Spray.genmodel* from the project's *model* folder
  1. Right click the genmodel's *root element* and select *Generate Model Code*

### 2. Step: Execute Language workflow
  1. Open the *org.eclipselabs.spray.xtext* project
  1. Right click on the *GenerateSpray.mwe2.launch* file and select *Run as => GenerateSpray.mew2*
  1. Check if the workflow finished with


    ```
    INFO  .emf.mwe2.runtime.workflow.Workflow  - Done.
    ```

## 4. Clean Projects

At the end, you have to clean your whole workspace to remove the errors:

  * Select *Project => Clean* and select *Clean all projects*
  * After cleaning the projects, Eclipse should automatically start to build the projects again
    * If not, select *Project => Build Automatically*


# Spray, Shapes and Styles Editors
## Launch Editors

  1. Open the *org.eclipselabs.spray.xtext* project
  1. Right click on the 

    * *SprayEditor-Mac.launch* if you are working on Mac OS X **OR**
    * *SprayEditor-Win.launch* if you are working on Windows

  1. Select *Run as => SprayEditor-Xxx*

A **2nd Eclipse instance** should start with deployed plugins for the Styles, Shapes and Spray editors.

## Create Spray Project
To create a graphical editor using the Spray Language you need two projects:
  
  * *myproject.domain* which contains your editor's ecore and genmodel
  * *myproject* which contains the *Styles*, *Shapes* and *Spray* code

### Create *myproject.domain* project

  1. Select *File => New => Other*
  2. Type *emf* in the search bar and select *Empty EMF Project*

![createEMFProject.png](https://bitbucket.org/repo/7R7eny/images/1915343597-createEMFProject.png)

  3. Follow the wizard
  4. Create your graphical editor's *Ecore* and *Genmodel*
  5. Generate the model code
    * Right click *myGenmodel.genmodel* and select *Generate Model Code*
    * The model code should be generated in the *emf-gen* folder if not you have the set it in your *myproject.genmodel* file 

### Create *myproject* Spray project

  1. Select *File => New => Other*
  2. Type *spray* in the search bar and select *Spray Project*

![createSprayProject.png](https://bitbucket.org/repo/7R7eny/images/2711733512-createSprayProject.png)

  3. Follow the wizard
  4. Describe your graphical editor with the *Spray*, *Shapes* and *Styles* language which can be found in the project's *model* folder.

# Graphiti Editor
## Launch generated graphical (Graphiti) Editor

  1. Open the *myproject* project
  1. Right click on the 

    * *GraphitiEditor-Mac.launch* if you are working on Mac OS X **OR**
    * *GraphitiEditor-Win.launch* if you are working on Windows

  1. Select *Run as => GraphitiEditor-Xxx*

A **3rd Eclipse instance** should start with your created graphical editor.

## Create Project
 
  1. Select *File => New => Other*
  1. Type *graphiti* in the search bar and select *Graphiti Sample Project*

![createGraphitiProject.png](https://bitbucket.org/repo/7R7eny/images/998323177-createGraphitiProject.png)

  1. Follow the wizard
  1. After creating the project switch to the Graphiti perspective.

**Remark:** Eclipse shows a tiny error in the *Error Log* after switching to the Graphiti perspective, you can't do anything against it, just ignore it ;-)

![Screen Shot 2014-06-12 at 11.12.30.png](https://bitbucket.org/repo/7R7eny/images/2639804191-Screen%20Shot%202014-06-12%20at%2011.12.30.png)


## Create Diagram
 
  1. Select *File => New => Other*
  1. Type *graphiti* in the search bar and select *Graphiti Diagram* (see last screenshot)
  1. Follow the wizard

Now you can start drawing a diagram with your generated graphical editor. Check out the **Petrinet Example** to see how it should looks like.



# Examples

Spray's GIT repository contains a *example_plugins* folder. All official examples should be stored there. At the moment you will find there a *petrinet* example.

## Petrinet

### 1. Step: Setup the *petrinet.domain* project

Instead of creating an *Empty EMF Project* as described above, you should use the provided project.

  1. Select * File => Import*
  1. Select * General => Existing Project into Workspace*
  1. Select the *org.eclipselabs.spray.examples.petrinet.domain* folder within *<spray repo>/example_plugins/* folder

After importing the project, you have to generate the model code

  1. Open the projects *model* folder  
  1. Right click *petrinet.genmodel* and select *Generate Model Code*
  1. The model code should be generated in the *emf-gen* folder

![petrinetDomainProject.png](https://bitbucket.org/repo/7R7eny/images/2518807331-petrinetDomainProject.png)

### 2. Step: Setup the *petrinet* spray project

  * Create the *petrinet spray project* as described above. The following screenshot will guide you through the spray wizard.

![petrinetSprayWizard.png](https://bitbucket.org/repo/7R7eny/images/196404072-petrinetSprayWizard.png)

  * You should now see the following.

![petrinetSprayProject.png](https://bitbucket.org/repo/7R7eny/images/814917319-petrinetSprayProject.png)

  * Replace the files in the project's *model* folder with the ones in the repository's petrinet example (*/example_plugins/petrinet/org.eclipselabs.spray.examples.petrinet/model*).

  * After the editor finished building the project open the project's *src-gen* folder.
  * Open the package *org.eclipselabs.spray.examples.petrinet.features*.
  * Select the two classes *PetrinetCustomShowClassNameFeature.java* and *PetrinetCustomShowInfoFeature.java*

![petrinetSprayCustom.png](https://bitbucket.org/repo/7R7eny/images/2491063271-petrinetSprayCustom.png)

  * Right click on the classes and select *Refactor => Move* and select the src folder
  * After the two files have been moved to the project's *src* folder replace them with the two Java files in the repository's petrinet example's src folder (*/example_plugins/petrinet/org.eclipselabs.spray.examples.petrinet/src/...*)

### 3. Step: Launch Petrinet Editor

Now you can start the graphical Graphiti Petrinet editor as described above

  1. Create a *Graphiti Sample Project* as described above
  1. Create a *Graphiti Diagram*
    * Select **petrinet** in the wizard

Now you should see an empty diagram and the *Palette* which contains the generated Shapes. That's it, now you should be able to drag and drop the elements out of the *Palette* and draw a Petrinet.

![petrinetDiagram.png](https://bitbucket.org/repo/7R7eny/images/3460500647-petrinetDiagram.png)