/*************************************************************************************
 *
 * Generated on Wed Jun 11 17:21:02 CEST 2014 by Spray CustomFeature.xtend
 * 
 * This file is an extension point: copy to "src" folder to manually add code to this
 * extension point.
 *
 *************************************************************************************/
package org.eclipselabs.spray.examples.petrinet.features;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.graphiti.features.IFeatureProvider;
import org.eclipse.graphiti.features.context.ICustomContext;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.widgets.Display;

public class PetrinetCustomShowClassNameFeature extends PetrinetCustomShowClassNameFeatureBase {
    public PetrinetCustomShowClassNameFeature(final IFeatureProvider fp) {
        super(fp);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void execute(final ICustomContext context, final EObject object) {
    	MessageDialog.openInformation(Display.getDefault().getActiveShell(), "Information", "Class Name "+ object.eClass().getClass().getSimpleName());
    }

}
