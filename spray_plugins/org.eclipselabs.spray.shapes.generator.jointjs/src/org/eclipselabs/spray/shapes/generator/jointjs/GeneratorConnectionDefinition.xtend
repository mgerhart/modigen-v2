package org.eclipselabs.spray.shapes.generator.jointjs

import java.util.ArrayList
import java.util.HashMap
import java.util.List
import java.util.Map
import javax.inject.Inject
import org.apache.commons.logging.Log
import org.apache.commons.logging.LogFactory
import org.eclipselabs.spray.shapes.CDEllipse
import org.eclipselabs.spray.shapes.CDLine
import org.eclipselabs.spray.shapes.CDPolygon
import org.eclipselabs.spray.shapes.CDPolyline
import org.eclipselabs.spray.shapes.CDRectangle
import org.eclipselabs.spray.shapes.CDRoundedRectangle
import org.eclipselabs.spray.shapes.CDText
import org.eclipselabs.spray.shapes.ConnectionDefinition
import org.eclipselabs.spray.shapes.PlacingDefinition
import org.eclipselabs.spray.shapes.ShapeConnection
import org.eclipselabs.spray.shapes.ShapestyleLayout
import org.eclipselabs.spray.styles.generator.jointjs.StyleGenerator
import org.eclipselabs.spray.shapes.TextLayout

class GeneratorConnectionDefinition {
	
	@Inject extension StyleGenerator styleGenerator
	private static Log   LOGGER       = LogFactory::getLog("ShapeGenerator");
	private Map<String, List<PlacingDefinition>> placingsCache = new HashMap<String, List<PlacingDefinition>>();
	private Map<String, List<PlacingDefinition>> labelCache = new HashMap<String, List<PlacingDefinition>>();
	
	def generate(Iterable<ConnectionDefinition> connections) {
		'''
		«head»
		function getConnectionStyle(stylename){
			var style;
			switch(stylename){
				«FOR connection : connections»
				case "«connection.name»":
					«IF connection.style != null && connection.style.dslStyle != null»
						//Get connection's default style
						style = getStyle("«connection.style.dslStyle.name»");
					«ELSE»
						//Default style for connection is empty
						style = {'.connection':{stroke: 'black'}};
					«ENDIF»
					«generateInlineStyle(connection)»
					«handlePlacings(connection)»
					break;
				«ENDFOR»				
				default:
					style = {};
					break;
			}
			
			return style;
		}
		
		function getPlacings(stylename){
			var placings;
			switch(stylename){
				«generateCachedPlacings»
				default:
					placings = [];
					break;
			}
			
			return placings;
		}
		
		function getLabels(stylename){
			var labels;
			switch(stylename){
				«generateCachedLabels»
				default:
					labels = [];
					break;
			}
			
			return labels;
		}
		'''
	}
	
	protected def head(){
		'''
		/*
		 * This is a generated ShapeFile for JointJS
		 */
		 '''			 
	}
	
	protected def generateInlineStyle(ConnectionDefinition connection){
		'''
		«IF connection.layout != null && connection.layout.layout != null»
			«val style = connection.layout.layout»
			//Get inline style
			var inline = {
				'.connection, .marker-target, .marker-source':{
					«styleGenerator.commonAttributes(style)»
					«styleGenerator.fontAttributes(style)»
				}
			};
			
			//Merge with default style
			jQuery.extend(style, inline);
		«ENDIF»
		'''
	}
	
	protected def handlePlacings(ConnectionDefinition connection){
		val placings = connection.placing
		var isTargetMarkerSet = false; //Check, whether a target marker is set, because JointJS will show an arrow if none is set		
		'''
		«FOR placing : placings»
			«IF placing.offset == 0.0»
				style['.marker-source'] = {
					«generateMarkerSourceCorrection()»
					«generateMarker(placing)»
				};
			«ELSEIF placing.offset == 1.0»
				style['.marker-target'] = {
					«generateMarker(placing)»
				};
				«{isTargetMarkerSet = true;""}»
			«ELSE»
				«cachePlacing(connection.name, placing)»
			«ENDIF»
		«ENDFOR»
		«IF !isTargetMarkerSet»
		style['.marker-target'] = {
			d: 'M 0 0' //override JointJS default arrow
		};
		«ENDIF»
		'''
	}
	
	protected def generateCachedPlacings(){
		val placings = '''
		«FOR entry : placingsCache.entrySet»
			case "«entry.key»":
				placings = [
					«FOR placing : entry.value»
					«generatePlacing(placing)»«IF placing != entry.value.last»,«ENDIF»
					«ENDFOR»
				];
				break;
		«ENDFOR»
		'''
		placingsCache.clear
		placings
	}
	
	protected def generateCachedLabels(){
		val labels = '''
		«FOR entry : labelCache.entrySet»
			case "«entry.key»":
				labels = [
					«FOR label : entry.value»
						«generateLabel(label)»
					«ENDFOR»
				];
				break;
		«ENDFOR»
		'''
		labelCache.clear
		labels
	}
	
	protected def generateLabel(PlacingDefinition placing){
		'''
		{
			position: «placing.offset»,
			attrs: {
				rect: {fill: 'transparent'},
				text: {dy: «placing.distance»}
			},
			id: '«(placing.shapeCon as CDText).body.value»'
		}
		'''
	}
	
	protected def generatePlacing(PlacingDefinition placing){
		'''
		{
			position: «placing.offset»,
			«generatePlacingShape(placing.shapeCon, placing.distance)»
		}'''
	}
	
	protected def dispatch generatePlacingShape(CDLine shape, int distance){
		'''
		markup: '<line />',
		attrs:{
			«IF shape.layout.layout != null»«getInlineStyle(shape.layout.layout)»«ENDIF»
			x1: «shape.layout.point.get(0).xcor»,
			y1: «shape.layout.point.get(0).ycor»,
			x2: «shape.layout.point.get(1).xcor»,
			y2: «shape.layout.point.get(1).ycor»
		}
		'''
	}
	
	protected def dispatch generatePlacingShape(CDPolyline shape, int distance){
		'''
		markup: '<polyline />',
		attrs:{
			«IF shape.layout.layout != null»«getInlineStyle(shape.layout.layout)»«ENDIF»
			«generateStyleCorrections(shape)»
			points: "«FOR point : shape.layout.point»«point.xcor», «point.ycor»«IF point != shape.layout.point.last» «ENDIF»«ENDFOR»"
		}
		'''
	}
	
	protected def dispatch generatePlacingShape(CDRectangle shape, int distance){
		'''
		markup: '<rect />',
		attrs:{
			«IF shape.layout.layout != null»«getInlineStyle(shape.layout.layout)»«ENDIF»
			height: «shape.layout.common.heigth»,
			width: «shape.layout.common.width»,
			y: «distance - shape.layout.common.heigth/2»
		}
		'''
	}
	
	protected def dispatch generatePlacingShape(CDRoundedRectangle shape, int distance){
		'''
		markup: '<rect />',
		attrs:{
			«IF shape.layout.layout != null»«getInlineStyle(shape.layout.layout)»«ENDIF»
			height: «shape.layout.common.heigth»,
			width: «shape.layout.common.width»,
			rx: «shape.layout.curveWidth»,
			ry: «shape.layout.curveHeight»,
			y: «distance - shape.layout.common.heigth/2»
		}
		'''
	}
	
	protected def dispatch generatePlacingShape(CDPolygon shape, int distance){
		'''
		markup: '<polygon />',
		attrs:{
			«IF shape.layout.layout != null»«getInlineStyle(shape.layout.layout)»«ENDIF»
			points: "«FOR point : shape.layout.point»«point.xcor», «point.ycor»«IF point != shape.layout.point.last» «ENDIF»«ENDFOR»"
		}
		'''
	}
	
	protected def dispatch generatePlacingShape(CDEllipse shape, int distance){
		'''
		markup: '<ellipse />',
		attrs:{
			«IF shape.layout.layout != null»«getInlineStyle(shape.layout.layout)»«ENDIF»
			rx: «shape.layout.common.width/2»,
			ry: «shape.layout.common.heigth/2»,
			cy: «distance»
		}
		'''
	}
	
	protected def dispatch generatePlacingShape(CDText shape, int distance){
		'''
		markup: '<text>«shape.body.value»</text>',
		attrs:{
			«IF shape.layout.layout != null»«getInlineStyle(shape.layout.layout)»«ENDIF»
			y: «shape.layout.common.heigth/2»
		}
		'''
	}
	
	
	protected def generateMarker(PlacingDefinition placing){
		'''
		«generateStyleCorrections(placing.shapeCon)»
		d: '«generateSvgPathData(placing.shapeCon)»' 
		'''
	}
	
	protected def dispatch generateSvgPathData(CDLine shape){
		val points = shape.layout.point
		'''M «points.get(0).xcor» «points.get(0).ycor» L «points.get(1).xcor» «points.get(1).ycor»'''
	}
	
	protected def dispatch generateSvgPathData(CDPolyline shape){
		val head = shape.layout.point.head
		val tail = shape.layout.point.tail
		'''M «head.xcor» «head.ycor» «FOR point : tail»L «point.xcor» «point.ycor»«ENDFOR»'''
	}
	
	protected def dispatch generateSvgPathData(CDRectangle shape){
		val common = shape.layout.common
		'''M «common.xcor» «common.ycor» l «common.width» 0 l 0 «common.heigth» l -«common.width» 0 z'''
	}
	
	protected def dispatch generateSvgPathData(CDRoundedRectangle shape){
		val layout = shape.layout
		val common = layout.common
		'''M «common.xcor + layout.curveWidth» «common.ycor + layout.curveHeight» l «common.width - 2*layout.curveWidth» 0 a «layout.curveWidth» «layout.curveHeight» 0 0 1 «layout.curveWidth» «layout.curveHeight» l 0 «common.heigth - 2*layout.curveHeight» a «layout.curveWidth» «layout.curveHeight» 0 0 1 -«layout.curveWidth» «layout.curveHeight» l -«common.width - 2*layout.curveWidth» 0 a «layout.curveWidth» «layout.curveHeight» 0 0 1 -«layout.curveWidth» -«layout.curveHeight» l 0 -«common.heigth - 2*layout.curveHeight» a «layout.curveWidth» «layout.curveHeight» 0 0 1 «layout.curveWidth» -«layout.curveHeight»'''
	}
	
	protected def dispatch generateSvgPathData(CDPolygon shape){
		val head = shape.layout.point.head
		val tail = shape.layout.point.tail
		'''M «head.xcor» «head.ycor» «FOR point : tail»L «point.xcor» «point.ycor»«ENDFOR» z'''
	}
	
	protected def dispatch generateSvgPathData(CDEllipse shape){
		val common = shape.layout.common
		val rx = common.width / 2
		val ry = common.heigth / 2
		'''M «common.xcor» «common.ycor» a  «rx» «ry» 0 0 1 «rx» -«ry» a  «rx» «ry» 0 0 1 «rx» «ry» a  «rx» «ry» 0 0 1 -«rx» «ry» a  «rx» «ry» 0 0 1 -«rx» -«ry» '''
	}
	
	protected def dispatch generateSvgPathData(CDText shape){
		''''''
	}
	
	protected def dispatch generateStyleCorrections(CDPolyline shape){
		'''
		fill: 'transparent', //JointJS uses fill attribute to fill in all markers
		'''
	}
	
	protected def dispatch generateStyleCorrections(ShapeConnection shape){
		''''''
	}
	
	protected def generateMarkerSourceCorrection() {
		'''transform: 'scale(1,1)','''
	}
	
	protected def void cachePlacing(String connection, PlacingDefinition placing){
		if(placing.shapeCon instanceof CDText){
			writeToCache(connection, placing, labelCache)
		}else{
			writeToCache(connection, placing, placingsCache)
		}
		
	}
	
	protected def writeToCache(String connection, PlacingDefinition placing, Map<String, List<PlacingDefinition>> cache) {
		if(cache.containsKey(connection)){
			cache.get(connection).add(placing);
		}else{
			val list = new ArrayList<PlacingDefinition>
			list.add(placing);
			cache.put(connection, list);
		}
	}
	
	protected def getInlineStyle(ShapestyleLayout layout){
		val style = layout.layout
		'''
		«IF style != null»
			«styleGenerator.commonAttributes(style)»
			«IF layout.eContainer.eClass == TextLayout»
				«styleGenerator.fontAttributes(style)»
			«ENDIF»
		«ENDIF»
		'''
	}
}