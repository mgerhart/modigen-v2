/** ****************************************************************************
 * Copyright (c)  The Spray Project.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Spray Dev Team - initial API and implementation
 **************************************************************************** */
 
 package org.eclipselabs.spray.shapes.generator.jointjs

import java.util.HashMap
import org.eclipselabs.spray.shapes.Shape
import org.apache.commons.logging.Log
import org.apache.commons.logging.LogFactory
import org.eclipselabs.spray.shapes.ShapeDefinition
import org.eclipselabs.spray.shapes.Line
import org.eclipselabs.spray.shapes.Rectangle
import org.eclipselabs.spray.shapes.Ellipse
import org.eclipselabs.spray.shapes.RoundedRectangle
import org.eclipselabs.spray.shapes.Text
import org.eclipselabs.spray.shapes.Polyline
import org.eclipselabs.spray.shapes.Polygon
import org.eclipselabs.spray.styles.generator.jointjs.StyleGenerator
import javax.inject.Inject
import org.eclipselabs.spray.shapes.ShapestyleLayoutimport org.eclipselabs.spray.shapes.HAlign
import org.eclipselabs.spray.shapes.TextLayout

class GeneratorShapeAndInlineStyle {
	
 	@Inject extension StyleGenerator styleGenerator
 	
 	private static Log LOGGER = LogFactory::getLog("GeneratorShapeAndInlineStyle");
 	
 	def shapeStyleHead() {
		'''
			function getShapeStyle(elementName) {
				
				var style;
				
				switch(elementName) {
		'''
	}
 	
	def generateShapeStyle(ShapeDefinition shapeDef, String packageName, Boolean LastElement, HashMap<String, HashMap<Shape, String>> attrs){
		if(shapeDef != null) {
			var att = attrs.get(shapeDef.name)
			'''
				case "«shapeDef.name»":
						«FOR shape : shapeDef.shape»
							«getShape(shape, att.get(shape))»
						«ENDFOR»
					break;
					
			'''		
		}
	} 	
	
	
	protected def dispatch getShape(Line shape, String shapeClass){
		'''
		«IF shape.style != null || shape.layout.layout != null»
			style = {'line.«shapeClass»':{} };
		«ENDIF»
		«IF shape.style != null»
			style['line.«shapeClass»'] = getStyle('«shape.style.dslStyle.name»').line;
		«ENDIF»
		«IF shape.layout.layout != null»
			style['line.«shapeClass»'] = {
				«getInlineStyle(shape.layout.layout)»
			};		
		«ENDIF»
		'''			
	}
	
	protected def dispatch getShape(Ellipse shape, String shapeClass){		
		'''
		«IF shape.style != null || shape.layout.layout != null»
			style = {'ellipse.«shapeClass»':{} };
		«ENDIF»
		«IF shape.style != null»
			style['ellipse.«shapeClass»'] = getStyle('«shape.style.dslStyle.name»').ellipse;
		«ENDIF»
		«IF shape.layout.layout != null»
			style['ellipse.«shapeClass»'] = {
				«getInlineStyle(shape.layout.layout)»
			};		
		«ENDIF»
		'''	
	}
	
	protected def dispatch getShape(Rectangle shape, String shapeClass){
		'''
		«IF shape.style != null || shape.layout.layout != null»
			style = {'rect.«shapeClass»':{} };
		«ENDIF»
		«IF shape.style != null»
			style['rect.«shapeClass»'] = getStyle('«shape.style.dslStyle.name»').rect;
		«ENDIF»
		«IF shape.layout.layout != null»
			style['rect.«shapeClass»'] = {
				«getInlineStyle(shape.layout.layout)»
			};		
		«ENDIF»
		'''
	}	
	
	protected def dispatch getShape(RoundedRectangle shape, String shapeClass){
		'''
		«IF shape.style != null || shape.layout.layout != null»
			style = {'rect.«shapeClass»':{} };
		«ENDIF»
		«IF shape.style != null»
			style['rect.«shapeClass»'] = getStyle('«shape.style.dslStyle.name»').rect;
		«ENDIF»
		«IF shape.layout.layout != null»
			style['rect.«shapeClass»'] = {
				«getInlineStyle(shape.layout.layout)»
			};		
		«ENDIF»
		'''		
	}	
	
	protected def dispatch getShape(Text shape, String shapeClass){
		'''
		«IF shape.style != null || shape.layout.layout != null»
			style = {'text.«shapeClass»':{} };
		«ENDIF»
		«IF shape.style != null»
			style['text.«shapeClass»'] = getStyle('«shape.style.dslStyle.name»').text;
		«ENDIF»
		«IF shape.layout.layout != null»
			style['text.«shapeClass»'] = {
				«getInlineStyle(shape.layout.layout)»
				ref-x: «getRefXValue(shape.layout.HAlign)»
			};		
		«ENDIF»
		'''		
	}		
	
	protected def dispatch getShape(Polyline shape, String shapeClass){
		'''
		«IF shape.style != null || shape.layout.layout != null»
			style = {'polyline.«shapeClass»':{} };
		«ENDIF»
		«IF shape.style != null»
			style['polyline.«shapeClass»'] = getStyle('«shape.style.dslStyle.name»').polyline;
		«ENDIF»
		«IF shape.layout.layout != null»
			style['polyline.«shapeClass»'] = {
				«getInlineStyle(shape.layout.layout)»
			};		
		«ENDIF»
		'''		
	}	
	
	protected def dispatch getShape(Polygon shape, String shapeClass){
		'''
		«IF shape.style != null || shape.layout.layout != null»
			style = {'polygon.«shapeClass»':{} };
		«ENDIF»
		«IF shape.style != null»
			style['polygon.«shapeClass»'] = getStyle('«shape.style.dslStyle.name»').polygon;
		«ENDIF»
		«IF shape.layout.layout != null»
			style['polygon.«shapeClass»'] = {
				«getInlineStyle(shape.layout.layout)»
			};		
		«ENDIF»
		'''				
	}		
		
	def shapeStyleFooter() {
		'''
					default:
						style = {};
					break;
					
				}
				return style;
			}
			
		'''
	}
	
	protected def getInlineStyle(ShapestyleLayout layout){
		val style = layout.layout
		'''
		«IF style != null»
			«styleGenerator.commonAttributes(style)»
			«IF layout.eContainer.eClass == TextLayout»
				«styleGenerator.fontAttributes(style)»
			«ENDIF»
		«ENDIF»
		'''
	}
	
	protected def getRefXValue(HAlign alignment){
		switch(alignment){
			case LEFT: 0
			case RIGHT: 1
			case CENTER: 0.5
			default: 0
		}
	}
 	
 }