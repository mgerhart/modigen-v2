package org.eclipselabs.spray.shapes.generator.jointjs

import org.eclipselabs.spray.shapes.ShapeDefinition
import org.eclipselabs.spray.shapes.Line
import org.eclipselabs.spray.shapes.Rectangle
import org.eclipselabs.spray.shapes.Ellipse
import org.eclipselabs.spray.shapes.Polygon
import org.eclipselabs.spray.shapes.Polyline
import org.eclipselabs.spray.shapes.RoundedRectangle
import org.eclipselabs.spray.shapes.Text
import java.util.UUID
import java.util.List
import java.util.HashMap
import org.eclipselabs.spray.shapes.Shape
import java.util.ArrayList
import org.eclipselabs.spray.styles.generator.jointjs.StyleGenerator
import javax.inject.Inject
import org.eclipselabs.spray.shapes.ShapestyleLayout
import org.eclipselabs.spray.shapes.Point
import org.eclipse.emf.common.util.EList
import org.eclipselabs.spray.shapes.ShapeContainerElement
import org.apache.commons.logging.Log
import org.apache.commons.logging.LogFactory

class GeneratorShapeDefinition {
	@Inject extension StyleGenerator styleGenerator
	private static Log   LOGGER       = LogFactory::getLog("ShapeGenerator");
	val attrs = new HashMap<String, List<CharSequence>>
	val attrsInspector = new HashMap<String, HashMap<Shape, String>>
	
	def getAttrsInspector() {
		return attrsInspector;
	}
	
	def head(String packageName) {
		'''
			/*
			 * This is a generated ShapeFile for JointJS
			 */
			
			if (typeof exports === 'object') {

				var joint = {
					util: require('../src/core').util,
					shapes: {
						basic: require('./joint.shapes.basic')
					},
					dia: {
						ElementView: require('../src/joint.dia.element').ElementView,
						Link: require('../src/joint.dia.link').Link
					}
				};
			}
			
			joint.shapes.«packageName» = {};
			
		'''
	}

	def generate(ShapeDefinition shapeDef, String packageName){
		'''
		joint.shapes.«packageName».«shapeDef.name» = joint.shapes.basic.Generic.extend({
			markup: «generateSvgMarkup(shapeDef)»,
			defaults: joint.util.deepSupplement({
				type: '«packageName».«shapeDef.name»',
				size: {width: «calculateWidth(shapeDef)», height: «calculateHeight(shapeDef)»},
				attrs: {
					'rect.bounding-box':{
						height: «calculateHeight(shapeDef)»,
						width: «calculateWidth(shapeDef)»
					},
					«generateAttrs(shapeDef.name)»
				}
			}, joint.dia.Element.prototype.defaults)
		});
		
		'''
	}
		
	protected def generateSvgMarkup(ShapeDefinition shapeDefinition){
		''' '<g class="rotatable"><g class="scalable"><rect class="bounding-box" />«FOR shape : shapeDefinition.shape»«generateSvgShape(shape, shapeDefinition.name, "scalable")»«ENDFOR»</g></g>' '''
	}
	
	protected def dispatch generateSvgShape(Line shape, String rootShapeName, String parentClass){
		val className = UUID.randomUUID
		buildAttrs(shape, rootShapeName, className.toString, parentClass)
		'''<line class="«className»" />'''
	}
	
	protected def dispatch generateSvgShape(Rectangle shape, String rootShapeName, String parentClass){
		val className = UUID.randomUUID
		buildAttrs(shape, rootShapeName, className.toString, parentClass)
		'''<rect class="«className»" />«FOR subshape : shape.shape»«generateSvgShape(subshape, rootShapeName, className.toString)»«ENDFOR»'''
	}
	
	protected def dispatch generateSvgShape(Ellipse shape, String rootShapeName, String parentClass){
		val className = UUID.randomUUID
		buildAttrs(shape, rootShapeName, className.toString, parentClass)
		'''<ellipse class="«className»" />«FOR subshape : shape.shape»«generateSvgShape(subshape, rootShapeName, className.toString)»«ENDFOR»'''
	}
	
	protected def dispatch generateSvgShape(Polygon shape, String rootShapeName, String parentClass){
		val className = UUID.randomUUID
		buildAttrs(shape, rootShapeName, className.toString, parentClass)
		'''<polygon class="«className»" />«FOR subshape : shape.shape»«generateSvgShape(subshape, rootShapeName, className.toString)»«ENDFOR»'''
	}
	
	protected def dispatch generateSvgShape(Polyline shape, String rootShapeName, String parentClass){
		val className = UUID.randomUUID
		buildAttrs(shape, rootShapeName, className.toString, parentClass)
		'''<polyline class="«className»" />'''
	}
	
	protected def dispatch generateSvgShape(RoundedRectangle shape, String rootShapeName, String parentClass){
		val className = UUID.randomUUID
		buildAttrs(shape, rootShapeName, className.toString, parentClass)
		'''<rect class="«className»" />«FOR subshape : shape.shape»«generateSvgShape(subshape, rootShapeName, className.toString)»«ENDFOR»'''
	}
	
	protected def dispatch generateSvgShape(Text shape, String rootShapeName, String parentClass){
		val className = UUID.randomUUID
		buildAttrs(shape, rootShapeName, className.toString, parentClass)
		'''<text class="«className» «shape.body.value»" />'''
	}
	
	protected def generateAttrs(String shapeName){
		val classes = attrs.get(shapeName)
		val text = '''
		«FOR clazz : classes»
			«clazz»«IF clazz != classes.last»,«ENDIF»
		«ENDFOR»
		'''
		attrs.clear
		text
	}
	
	protected def buildAttrs(Shape shape, String shapeName, String className, String parentClass){
		
		val attributes = '''
			'.«className»':{
				«getAttributes(shape, parentClass)»
			}'''
		
		if(attrs.containsKey(shapeName)){
			attrs.get(shapeName).add(attributes)		
			attrsInspector.get(shapeName).put(shape,className)
		}else{
			val list = new ArrayList()
			list.add(attributes)
			
			val att = new HashMap<Shape, String>
			att.put(shape, className)
			attrs.put(shapeName, list)
			attrsInspector.put(shapeName, att)
		}
		
	}
	
	protected def dispatch getAttributes(Line shape, String parentClass){
		'''
		x1: «shape.x1»,
		y1: «shape.y1»,
		x2: «shape.x2»,
		y2: «shape.y2»
		'''
	}
	
	protected def dispatch getAttributes(Rectangle shape, String parentClass){
		'''
		«generatePosition(shape)»
		'width': «shape.layout.common.width»,
		'height': «shape.layout.common.heigth»
		'''
	}
	
	protected def dispatch getAttributes(Ellipse shape, String parentClass){
		'''
		«generatePosition(shape)»
		rx: «shape.layout.common.width / 2»,
		ry: «shape.layout.common.heigth / 2»
		'''
	}
	
	protected def dispatch getAttributes(Polygon shape, String parentClass){
		'''
		points: "«FOR point : shape.layout.point»«getX(point, shape)»,«getY(point, shape)» «ENDFOR»"
		'''
	}
	
	protected def dispatch getAttributes(Polyline shape, String parentClass){
		'''
		points: "«FOR point : shape.layout.point»«getX(point, shape)»,«getY(point, shape)» «ENDFOR»"
		'''
	}
	
	protected def dispatch getAttributes(RoundedRectangle shape, String parentClass){
		'''
		«generatePosition(shape)»
		'width': «shape.layout.common.width»,
		'height': «shape.layout.common.heigth»,
		rx: «shape.layout.curveWidth»,
		ry: «shape.layout.curveHeight»
		'''
	}
	
	protected def dispatch getAttributes(Text shape, String parentClass){
		'''
		«generatePosition(shape)»
		'width': «shape.layout.common.width»,
		'height': «shape.layout.common.heigth»,
		text: "«shape.body.value»" //Is overwritten in stencil, but needed here for scaling
		'''
	}
	
	protected def generatePosition(Rectangle shape){
		'''
		x: «shape.x»,
		y: «shape.y»,
		'''
	}
	
	protected def generatePosition(Ellipse shape){
		'''
		cx: «shape.cx»,
		cy: «shape.cy»,
		'''
	}
	
	protected def generatePosition(RoundedRectangle shape){
		'''
		x: «shape.x»,
		y: «shape.y»,
		'''
	}
	
	protected def generatePosition(Text shape){
		'''
		x: «shape.x»,
		y: «shape.y»,
		'''
	}
	
	protected def calculateWidth(ShapeDefinition shapeDef){
		var int width = 0
		for (Shape s : shapeDef.shape){
			if(s.width > width){
				width = s.width
			}
		}
		width	
	}
	
	protected def dispatch getWidth(Line shape){maxX(shape.layout.point)}
	protected def dispatch getWidth(Rectangle shape){shape.layout.common.xcor + shape.layout.common.width}
	//Ellipse is automatically positioned with xcor + radius. Thus, we need xcor + radius + radius = xcor + diameter here.
	protected def dispatch getWidth(Ellipse shape){shape.layout.common.xcor + shape.layout.common.width}
	protected def dispatch getWidth(Polygon shape){maxX(shape.layout.point)}
	protected def dispatch getWidth(Polyline shape){maxX(shape.layout.point)}
	protected def dispatch getWidth(RoundedRectangle shape){shape.layout.common.xcor + shape.layout.common.width}
	protected def dispatch getWidth(Text shape){shape.layout.common.xcor + shape.layout.common.width}
	
	protected def maxX(EList<Point> points){
		var max = points.head.xcor
		for(p : points){
			if(p.xcor > max){
				max = p.xcor
			}
		}
		max
	}
	
	protected def minX(EList<Point> points){
		var min = points.head.xcor
		for(p : points){
			if(p.xcor < min){
				min = p.xcor
			}
		}
		min
	}
	
	protected def calculateHeight(ShapeDefinition shapeDef){
		var int height = 0
		for (Shape s : shapeDef.shape){
			if(s.height > height){
				height = s.height
			}
		}
		height
	}
	
	protected def dispatch getHeight(Line shape){maxY(shape.layout.point)}
	protected def dispatch getHeight(Rectangle shape){shape.layout.common.ycor + shape.layout.common.heigth}
	//Ellipse is automatically positioned with ycor + radius. Thus we need ycor + radius + radius = ycor + diameter here
	protected def dispatch getHeight(Ellipse shape){shape.layout.common.ycor + shape.layout.common.heigth}
	protected def dispatch getHeight(Polygon shape){maxY(shape.layout.point)}
	protected def dispatch getHeight(Polyline shape){maxY(shape.layout.point)}
	protected def dispatch getHeight(RoundedRectangle shape){shape.layout.common.ycor + shape.layout.common.heigth}
	protected def dispatch getHeight(Text shape){shape.layout.common.ycor + shape.layout.common.heigth}
	
	protected def maxY(EList<Point> points){
		var max = points.head.ycor
		for(p : points){
			if(p.ycor > max){
				max = p.ycor
			}
		}
		max
	}
	
	protected def minY(EList<Point> points){
		var min = points.head.ycor
		for(p : points){
			if(p.ycor < min){
				min = p.ycor
			}
		}
		min
	}
	
	protected def getX(Point point, Shape shape){
		if(shape.isRoot){
			point.xcor
		}else{
			point.xcor + referenceX(shape.parent)
		}
	}
	
	protected def getY(Point point, Shape shape){
		if(shape.isRoot){
			point.ycor
		}else{
			point.ycor + referenceY(shape.parent)
		}
	}
	
	protected def getX1(Line shape){
		if(shape.isRoot){
			shape.layout.point.get(0).xcor
		}else{
			shape.layout.point.get(0).xcor + referenceX(shape.parent)
		}
	}
	
	protected def getX2(Line shape){
		if(shape.isRoot){
			shape.layout.point.get(1).xcor
		}else{
			shape.layout.point.get(1).xcor + referenceX(shape.parent)
		}
	}
	
	protected def getY1(Line shape){
		if(shape.isRoot){
			shape.layout.point.get(0).ycor
		}else{
			shape.layout.point.get(0).ycor + referenceX(shape.parent)
		}
	}
	
	protected def getY2(Line shape){
		if(shape.isRoot){
			shape.layout.point.get(1).ycor
		}else{
			shape.layout.point.get(1).ycor + referenceX(shape.parent)
		}
	}
	
	protected def getX(Rectangle shape){
		if(shape.isRoot){
			shape.layout.common.xcor
		}else{
			shape.layout.common.xcor + referenceX(shape.parent)
		}
	}
	
	protected def getY(Rectangle shape){
		if(shape.isRoot){
			shape.layout.common.ycor
		}else{
			shape.layout.common.ycor + referenceY(shape.parent)
		}
	}
	
	protected def getX(RoundedRectangle shape){
		if(shape.isRoot){
			shape.layout.common.xcor
		}else{
			shape.layout.common.xcor + referenceX(shape.parent)
		}
	}
	
	protected def getY(RoundedRectangle shape){
		if(shape.isRoot){
			shape.layout.common.ycor
		}else{
			shape.layout.common.ycor + referenceY(shape.parent)
		}
	}
	
	protected def getX(Text shape){
		if(shape.isRoot){
			shape.layout.common.xcor
		}else{
			shape.layout.common.xcor + referenceX(shape.parent)
		}
	}
	
	protected def getY(Text shape){
		if(shape.isRoot){
			shape.layout.common.ycor
		}else{
			shape.layout.common.ycor + referenceY(shape.parent)
		}
	}
	
	protected def getCx(Ellipse shape){
		if(shape.isRoot){
			(shape.layout.common.width /2) + shape.layout.common.xcor
		}else{
			(shape.layout.common.width /2) + shape.layout.common.xcor + referenceX(shape.parent)
		}
	}
	
	protected def getCy(Ellipse shape){
		if(shape.isRoot){
			(shape.layout.common.heigth /2) + shape.layout.common.ycor
		}else{
			(shape.layout.common.heigth /2) + shape.layout.common.ycor + referenceY(shape.parent)
		}
	}
	
	protected def dispatch int referenceX (Rectangle shape){
		if(shape.isRoot){
			shape.layout.common.xcor
		}else{
			shape.layout.common.xcor + referenceX(shape.parent)
		}
	}
	
	protected def dispatch int referenceX (Ellipse shape){
		if(shape.isRoot){
			shape.layout.common.xcor
		}else{
			shape.layout.common.xcor + referenceX(shape.parent)
		}
	}
	
	protected def dispatch int referenceX (Polygon shape){
		if(shape.isRoot){
			minX(shape.layout.point)
		}else{
			minX(shape.layout.point) + referenceX(shape.parent)
		}
	}
	
	protected def dispatch int referenceX (RoundedRectangle shape){
		if(shape.isRoot){
			shape.layout.common.xcor
		}else{
			shape.layout.common.xcor + referenceX(shape.parent)
		}
	}
	
	protected def dispatch int referenceY (Rectangle shape){
		if(shape.isRoot){
			shape.layout.common.ycor
		}else{
			shape.layout.common.ycor + referenceY(shape.parent)
		}
	}
	
	protected def dispatch int referenceY (Ellipse shape){
		if(shape.isRoot){
			shape.layout.common.ycor
		}else{
			shape.layout.common.ycor + referenceY(shape.parent)
		}
	}
	
	protected def dispatch int referenceY (Polygon shape){
		if(shape.isRoot){
			minY(shape.layout.point)
		}else{
			minY(shape.layout.point) + referenceY(shape.parent)
		}
	}
	
	protected def dispatch int referenceY (RoundedRectangle shape){
		if(shape.isRoot){
			shape.layout.common.ycor
		}else{
			shape.layout.common.ycor + referenceY(shape.parent)
		}
	}

	
	protected def isRoot(Shape shape){shape.eContainer instanceof ShapeContainerElement}
	protected def getParent(Shape shape){
		if(shape.isRoot){
			null
		}else{
			shape.eContainer as Shape
		}
	}	
}