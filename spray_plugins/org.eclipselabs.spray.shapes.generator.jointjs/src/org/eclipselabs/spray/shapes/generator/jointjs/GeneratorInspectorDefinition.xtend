package org.eclipselabs.spray.shapes.generator.jointjs

import java.util.HashMap
import org.apache.commons.logging.Log
import org.apache.commons.logging.LogFactory
import org.eclipselabs.spray.shapes.Ellipse
import org.eclipselabs.spray.shapes.Line
import org.eclipselabs.spray.shapes.Polygon
import org.eclipselabs.spray.shapes.Polyline
import org.eclipselabs.spray.shapes.Rectangle
import org.eclipselabs.spray.shapes.RoundedRectangle
import org.eclipselabs.spray.shapes.Shape
import org.eclipselabs.spray.shapes.ShapeContainerElement
import org.eclipselabs.spray.shapes.Text
import com.google.inject.Inject
import org.eclipselabs.spray.shapes.ShapeDefinition

class GeneratorInspectorDefinition {
	
	private static Log   LOGGER       = LogFactory::getLog("ShapeGenerator");
	@Inject extension GeneratorShapeDefinition shapeGenerator
	
	def generate(ShapeContainerElement shape, String packageName, Boolean lastElement, HashMap<String, HashMap<Shape, String>> attrs) {
		val atts = attrs.get(shape.name)
		var boundWidth = 0
		var boundHeight = 0
		
		if(shape instanceof ShapeDefinition){
			boundWidth = shapeGenerator.calculateWidth(shape)
			boundHeight = shapeGenerator.calculateHeight(shape)
		} 
		
		if(atts != null) {
			val attsize = atts.size
			var counter = 1	
		'''
			'«packageName».«shape.name»':{
				inputs: _.extend({
					attrs: {
						«FOR att : atts.keySet»
							«IF attsize != counter» 
							«getAttributes(att, atts.get(att), false, boundWidth, boundHeight)»
							«ELSE»
							«getAttributes(att, atts.get(att), true, boundWidth, boundHeight)»
							«ENDIF»
							«{counter=counter+1;""}»
						«ENDFOR»
					}
				}, CommonInspectorInputs),
				groups: CommonInspectorGroups
			},
		'''
		}
	}
		
	def head() {
		'''
		var InspectorDefs = {
			// FSA
			// ---
		'''
	}
	
	def footer() {
		'''
		«linkAttributes»
		};
		'''
	}
	
	def dispatch getAttributes(Rectangle shape, String shapeClass, Boolean last, int maxWidth, int maxHeight) {
		'''
		'rect«IF shape.hasShapeOrInlineStyle».«shapeClass»«ENDIF»': inp({
			fill: { group: 'Presentation Rectangle', index: 1, label: 'Background-Color Rectangle' },
			'fill-opacity': {group: 'Presentation Rectangle', index: 2, label: 'Opacity Rectangle'},
			stroke: {group: 'Presentation Rectangle', index: 3, label: 'Line-Color Rectangle'},
			'stroke-width': { group: 'Presentation Rectangle', index: 4, min: 0, max: 30, defaultValue: 1 },
			'stroke-dasharray': { group: 'Presentation Rectangle', index: 5, label: 'Stroke Dash Rectangle' }
		}),
		'.«shapeClass»': inp({
			x: {group: 'Geometry Rectangle', index: 1, max:«maxWidth - shape.layout.common.width», label: 'x Position Rectangle'},
			y: {group: 'Geometry Rectangle', index: 2, max:«maxHeight - shape.layout.common.heigth», label: 'y Position Rectangle'},
			height: {group: 'Geometry Rectangle', index: 3, max:«maxHeight», label: 'Height Rectangle'},
			width: {group: 'Geometry Rectangle', index: 3, max:«maxWidth», label: 'Width Rectangle'}
		})«IF !last»,«ENDIF»
		'''
	}
	
	def dispatch getAttributes(Text shape, String shapeClass, Boolean last, int maxWidth, int maxHeight) {
		'''
		'.«shape.body.value»': inp({text: { group: 'text', index: 1 }}),
		'text«IF shape.hasShapeOrInlineStyle».«shapeClass»«ENDIF»' : inp({
			'font-size': { group: 'text', index: 2 },
			'font-family': { group: 'text', index: 3 },
			'font-weight': { group: 'text', index: 4 },
			fill: { group: 'text', index: 6, label:'Text Color' },
		}),
		'.«shapeClass»': inp({
			x: {group: 'Text Geometry', index: 1, max:«maxWidth - shape.layout.common.width», label: 'x Position Text'},
			y: {group: 'Text Geometry', index: 2, max:«maxHeight - shape.layout.common.heigth», label: 'y Position Text'}
		})«IF !last»,«ENDIF»
		'''
	}
	
	def dispatch getAttributes(Line shape, String shapeClass, Boolean last, int maxWidth, int maxHeight) {
		'''
		'line«IF shape.hasShapeOrInlineStyle».«shapeClass»«ENDIF»' : inp({
			stroke: { group: 'Presentation', index: 2, label: 'Line-Color' },
			'stroke-width': { group: 'Presentation', index: 3, min: 0, max: 30, defaultValue: 1, label: ' Stroke Width Line' },
			'stroke-dasharray': { group: 'Presentation', index: 4, label: 'Stroke Dash Line' }
		})«IF !last»,«ENDIF»
		'''
	}
	
	def dispatch getAttributes(Ellipse shape, String shapeClass, Boolean last, int maxWidth, int maxHeight) {
		'''
		'ellipse«IF shape.hasShapeOrInlineStyle».«shapeClass»«ENDIF»' : inp({
			fill: { group: 'Presentation', index: 1, label:'Background-Color Ellipse' },
			'fill-opacity': {group: 'Presentation', index: 2, label: 'Opacity Ellipse'},
			stroke: {group: 'Presentation', index: 3, label: 'Line-Color Rectangle'},
			'stroke-width': { group: 'Presentation', index: 4, min: 0, max: 30, defaultValue: 1, label: 'Stroke Width Ellipse' },
			'stroke-dasharray': { group: 'Presentation', index: 5, label: 'Stroke Dash Ellipse' }
		}),
		'.«shapeClass»': inp({
			cx: {group: 'Geometry Ellipse', index: 1, min: «shape.layout.common.width/2», max:«maxWidth - shape.layout.common.width/2»},
			cy: {group: 'Geometry Ellipse', index: 2, min: «shape.layout.common.heigth/2», max:«maxHeight - shape.layout.common.heigth/2»},
			rx: {group: 'Geometry Ellipse', index: 3, max:«maxWidth/2»},
			ry: {group: 'Geometry Ellipse', index: 3, max:«maxHeight/2»}
		})«IF !last»,«ENDIF»
		'''
	}
	
	def dispatch getAttributes(Polygon shape, String shapeClass, Boolean last, int maxWidth, int maxHeight) {
		'''
		'polygon«IF shape.hasShapeOrInlineStyle».«shapeClass»«ENDIF»' : inp({
			fill: { group: 'Presentation', index: 1, label:'Background-Color Polygon' },
			'fill-opacity': {group: 'Presentation', index: 2, label: 'Opacity Polygon'},
			stroke: {group: 'Presentation', index: 3, label: 'Line-Color Rectangle'},
			'stroke-width': { group: 'Presentation', index: 4, min: 0, max: 30, defaultValue: 1,label: 'Stroke Width Polygon' },
			'stroke-dasharray': { group: 'Presentation', index: 5, label: 'Stroke Dash Polygon' }
		})«IF !last»,«ENDIF»
		'''
	}
		
	def dispatch getAttributes(Polyline shape, String shapeClass, Boolean last, int maxWidth, int maxHeight) {
		'''
		'polyline«IF shape.hasShapeOrInlineStyle».«shapeClass»«ENDIF»' : inp({
			fill: { group: 'Presentation', index: 1, label:'Background-Color Polyline' },
		})«IF !last»,«ENDIF»
		'''
	}	
		
	def dispatch getAttributes(RoundedRectangle shape, String shapeClass, Boolean last, int maxWidth, int maxHeight) {
		'''
		'rect«IF shape.hasShapeOrInlineStyle».«shapeClass»«ENDIF»' : inp({
			fill: { group: 'Presentation Rounded Rectangle', index: 1 },
			'fill-opacity': {group: 'Presentation Rounded Rectangle', index: 2, label: 'Opacity Rounded Rectangle'},
			stroke: {group: 'Presentation Rounded Rectangle', index: 3, label: 'Line-Color Rectangle'},
			'stroke-width': { group: 'Presentation Rounded Rectangle', index: 4, min: 0, max: 30, defaultValue: 1, label: 'Stroke Width Rounded Rectangle' },
			'stroke-dasharray': { group: 'Presentation Rounded Rectangle', index: 5, label: 'Stroke Dash Rounded Rectangle' },
		}),
		'.«shapeClass»': inp({
			rx: { group: 'Geometry Rounded Rectangle', index: 6, max:«shape.layout.common.width/2», label: 'Curve X' },
			ry: { group: 'Geometry Rounded Rectangle', index: 7, max:«shape.layout.common.heigth/2», label: 'Curve Y' },
			x: {group: 'Geometry Rounded Rectangle', index: 1, max:«maxWidth - shape.layout.common.width», label: 'x Position Rounded Rectangle'},
			y: {group: 'Geometry Rounded Rectangle', index: 2, max:«maxHeight - shape.layout.common.heigth», label: 'y Position Rounded Rectangle'},
			height: {group: 'Geometry Rounded Rectangle', index: 3, max:«maxHeight», label: 'Height Rounded Rectangle'},
			width: {group: 'Geometry Rounded Rectangle', index: 3, max:«maxWidth», label: 'Width Rounded Rectangle'}
		})«IF !last»,«ENDIF»
		'''
	}
	
	def getLinkAttributes(){
		'''
		'modigen.MLink':{
			inputs:{
				labels:{
					type: 'list',
					group: 'labels',
					attrs:{
						label: {'data-tooltip': 'Set (possibly multiple) labels for the link'}
					},
					item:{
						type: 'object',
						properties: {
							position: { type: 'range', min: 0.1, max: .9, step: .1, defaultValue: .5, label: 'position', index: 2, attrs: { label: { 'data-tooltip': 'Position the label relative to the source of the link' } } },
							attrs: {
								text: {
									text: { type: 'text', label: 'text', defaultValue: 'label', index: 1, attrs: { label: { 'data-tooltip': 'Set text of the label' } } }
								}
							} 
						}
					}
				}
			},
			groups:{
				labels: {label: 'Labels', index: 1}
			}
		}
		'''
	}
	
	protected def hasShapeOrInlineStyle(Shape shape){
		return shape.style != null && shape.style.dslStyle != null || shape.hasInlineStyle
	}
	
	protected def dispatch hasInlineStyle(Line shape){
		return shape.layout !=null && shape.layout.layout != null && shape.layout.layout.layout != null
	}
	
	protected def dispatch hasInlineStyle(Polyline shape){
		return shape.layout !=null && shape.layout.layout != null && shape.layout.layout.layout != null
	}
	
	protected def dispatch hasInlineStyle(Polygon shape){
		return shape.layout !=null && shape.layout.layout != null && shape.layout.layout.layout != null
	}
	
	protected def dispatch hasInlineStyle(Rectangle shape){
		return shape.layout !=null && shape.layout.layout != null && shape.layout.layout.layout != null
	}
	
	protected def dispatch hasInlineStyle(RoundedRectangle shape){
		return shape.layout !=null && shape.layout.layout != null && shape.layout.layout.layout != null
	}
	
	protected def dispatch hasInlineStyle(Ellipse shape){
		return shape.layout !=null && shape.layout.layout != null && shape.layout.layout.layout != null
	}
	
	protected def dispatch hasInlineStyle(Text shape){
		return shape.layout !=null && shape.layout.layout != null && shape.layout.layout.layout != null
	}
}