package org.eclipselabs.spray.xtext.ui.syntaxcoloring;

import java.util.HashSet;

import org.eclipse.xtext.ui.editor.syntaxcoloring.DefaultAntlrTokenToAttributeIdMapper;

public class SprayTokenToAttributeIdMapper extends DefaultAntlrTokenToAttributeIdMapper {

	private static String DIAGRAM_TOKEN = "'diagram'";
	private static String FOR_TOKEN = "'for'";
	private static String NODE_TOKEN = "'node'";
	private static String EDGE_TOKEN = "'edge'";	
	private static String JAVA_TOKEN = "'JAVA'";
	
	private HashSet<String> parameterTokens = new HashSet<String>();	
	private HashSet<String> actionTokens = new HashSet<String>();
	private HashSet<String> attributeTokens = new HashSet<String>();
	
	public SprayTokenToAttributeIdMapper(){
		parameterTokens.add("'style'");
		parameterTokens.add("'label'");
		parameterTokens.add("'implementation'");
		parameterTokens.add("'var'");
		parameterTokens.add("'val'");
		parameterTokens.add("'nest'");
		
		actionTokens.add("'actionGroup'");
		actionTokens.add("'action'");
		actionTokens.add("'include'");
		
		attributeTokens.add("'actions'");
		attributeTokens.add("'shape'");
		attributeTokens.add("'connection'");
		attributeTokens.add("'palette'");
		attributeTokens.add("'container'");
		attributeTokens.add("'onCreate'");
		attributeTokens.add("'askFor'");
		attributeTokens.add("'from'");
		attributeTokens.add("'to'");
		attributeTokens.add("'for'");
	}
	
	@Override
	protected String calculateId(String tokenName, int tokenType) {
		if(tokenName.equals(DIAGRAM_TOKEN)){
			return SprayHighlightingConfiguration.DIAGRAM_ID;
		}
		if(tokenName.equals(EDGE_TOKEN) || tokenName.equals(NODE_TOKEN)){
			return SprayHighlightingConfiguration.METACLASS_ID;
		}
		if(tokenName.equals(JAVA_TOKEN)){
			return SprayHighlightingConfiguration.JAVA_ID;
		}
		if(actionTokens.contains(tokenName)){
			return SprayHighlightingConfiguration.ACTION_ID;
		}
		if(parameterTokens.contains(tokenName)){
			return SprayHighlightingConfiguration.PARAMETER_ID;
		}
		if(attributeTokens.contains(tokenName)){
			return SprayHighlightingConfiguration.ATTRIBUTE_ID;
		}
		return super.calculateId(tokenName, tokenType);
	}
}
