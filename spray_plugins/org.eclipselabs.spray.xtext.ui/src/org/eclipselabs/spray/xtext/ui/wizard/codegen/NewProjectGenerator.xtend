/** ****************************************************************************
 * Copyright (c)  The Spray Project.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Spray Dev Team - initial API and implementation
 **************************************************************************** */
package org.eclipselabs.spray.xtext.ui.wizard.codegen

import javax.inject.Inject
import org.eclipse.xtext.generator.IFileSystemAccess
import org.eclipselabs.spray.xtext.ui.wizard.SprayProjectInfo

class NewProjectGenerator {
    @Inject SprayModelGenerator generateModel

    def doGenerate (SprayProjectInfo projectInfo, IFileSystemAccess fsa) {
        generateModel.doGenerate(projectInfo, fsa)

	    generateBuildProperties(projectInfo, fsa)
	    generateAntUpdatePluginXmlLaunch(projectInfo, fsa)
	    generateAntUpdatePluginXml(projectInfo, fsa)
	    generateGraphitiEditorLaunchMac(projectInfo, fsa)
	    generateGraphitiEditorLaunchWin(projectInfo, fsa)
    }

    def generateBuildProperties (SprayProjectInfo pi, IFileSystemAccess fsa) {
       val content = '''
        source.. = «pi.javaMainSrcDir»/,\
                   «pi.javaGenSrcDir»/,\
                   «pi.sprayModelDir»/
        bin.includes = META-INF/,\
        			   ant-update-plugin-xml.xml,\
                       plugin.xml,\
                       icons/,\
                       «pi.sprayModelDir»/
       '''
       fsa.generateFile("build.properties", pi.projectName, content);
    }

    def generateAntUpdatePluginXml(SprayProjectInfo pi, IFileSystemAccess fsa) {
		val content = '''
			<!-- see http://5ise.quanxinquanyi.de/2013/01/11/avoiding-manual-merge-when-generating-plugin-xml-with-xtextxtend/ -->
			<project default="updateAll">

				<target name="findFile">
					<fileset id="matches" dir="${basedir}">
					    <filename name="plugin.xml"/>
					</fileset>
					<condition property="foundFile">
						<resourcecount when="greater" count="0" refid="matches" />
					</condition>
				</target>

				<target name="ensureFileExists" depends="findFile" unless="foundFile">
					<copy file="src-gen/plugin.xml" todir="${basedir}" />
					<antcall target="findFile" />
				</target>

				<target name="updateAll" depends="ensureFileExists" if="foundFile">

					«generateAntCall("plugin_diagramtype.xml", "DIAGRAM TYPE")»

					«generateAntCall("plugin_diagramtypeprovider.xml", "DIAGRAM TYPE PROVIDER")»

					«generateAntCall("plugin_graphitieditor.xml", "GRAPHITI EDITOR EXTENSION")»

					«generateAntCall("plugin_imageprovider.xml", "IMAGE PROVIDER")»

					«generateAntCall("plugin_newdiagramwizard.xml", "NEW DIAGRAM WIZARD")»

					«generateAntCall("plugin_propertysections.xml", "PROPERTY SECTIONS")»

					«generateAntCall("plugin_propertytabs.xml", "PROPERTY TAB")»

				</target>

				<target name="findInFile">
					<fileset id="matches" dir="${basedir}">
					    <filename name="plugin.xml"/>
						<containsregexp
							expression="${commentSegment}"/>
					</fileset>
					<condition property="foundInFile">
						<resourcecount when="greater" count="0" refid="matches" />
					</condition>
				</target>

				<target name="readSegmentFileContent" depends="findInFile" if="foundInFile">
					<loadfile property="segmentFileContent" srcfile="${basedir}/src-gen/${segmentFileName}" />
				</target>

				<target name="updateWithContent" depends="readSegmentFileContent" if="segmentFileContent">

					<replaceregexp
						file="plugin.xml"
						match="${commentMatchString}"
						replace="&lt;!-- ${commentSegment} AUTOGEN START -->${line.separator}
							${segmentFileContent}
							&lt;!-- ${commentSegment} AUTOGEN END -->"
						flags="gis"
						byline="false"
					/>
					<echo message="Regenerating content of ${commentMatchString} in plugin.xml with content from ${segmentFileName}" />
				</target>

				<target name="updateWithEmptyContent" depends="readSegmentFileContent" unless="segmentFileContent">
					<replaceregexp
						file="plugin.xml"
						match="${commentMatchString}"
						replace="&lt;!-- ${commentSegment} AUTOGEN START -->${line.separator}
						&lt;!-- ${commentSegment} AUTOGEN END -->"
						flags="gis"
						byline="false"
					/>
				</target>

				<target name="update" depends="updateWithEmptyContent, updateWithContent" />

			</project>
		'''
       fsa.generateFile("ant-update-plugin-xml.xml", pi.projectName, content);
    }

    def private generateAntCall(String segmentFileName, String commentSegment) '''
		<antcall target="update">
			<param name="segmentFileName" value="«segmentFileName»" />
			<param name="commentSegment" value="«commentSegment»" />
			<param name="commentMatchString" value="&lt;!-- «commentSegment» AUTOGEN START --&gt;(.+?)\&lt;!-- «commentSegment» AUTOGEN END --&gt;" />
		</antcall>
    '''

    def generateAntUpdatePluginXmlLaunch(SprayProjectInfo pi, IFileSystemAccess fsa) {
    	val content = '''
			<?xml version="1.0" encoding="UTF-8" standalone="no"?>
			<launchConfiguration type="org.eclipse.ant.AntBuilderLaunchConfigurationType">
				<booleanAttribute key="org.eclipse.ant.ui.ATTR_TARGETS_UPDATED" value="true"/>
				<booleanAttribute key="org.eclipse.ant.ui.DEFAULT_VM_INSTALL" value="false"/>
				<booleanAttribute key="org.eclipse.debug.ui.ATTR_LAUNCH_IN_BACKGROUND" value="false"/>
				<stringAttribute key="org.eclipse.jdt.launching.CLASSPATH_PROVIDER" value="org.eclipse.ant.ui.AntClasspathProvider"/>
				<booleanAttribute key="org.eclipse.jdt.launching.DEFAULT_CLASSPATH" value="true"/>
				<stringAttribute key="org.eclipse.jdt.launching.PROJECT_ATTR" value="«pi.projectName»"/>
				<stringAttribute key="org.eclipse.ui.externaltools.ATTR_LOCATION" value="${workspace_loc:/«pi.projectName»/ant-update-plugin-xml.xml}"/>
				<stringAttribute key="org.eclipse.ui.externaltools.ATTR_RUN_BUILD_KINDS" value="full,incremental,"/>
				<booleanAttribute key="org.eclipse.ui.externaltools.ATTR_TRIGGERS_CONFIGURED" value="true"/>
				<stringAttribute key="org.eclipse.ui.externaltools.ATTR_WORKING_DIRECTORY" value="${workspace_loc:/«pi.projectName»}"/>
			</launchConfiguration>
    	'''
       fsa.generateFile("/.externalToolBuilders/UpdatePlugin.xml.launch", pi.projectName, content);
    }
    
    def generateGraphitiEditorLaunchMac(SprayProjectInfo pi, IFileSystemAccess fsa) {
    	val content = '''
			<?xml version="1.0" encoding="UTF-8" standalone="no"?>
			<launchConfiguration type="org.eclipse.pde.ui.RuntimeWorkbench">
				<booleanAttribute key="append.args" value="true"/>
				<booleanAttribute key="askclear" value="true"/>
				<booleanAttribute key="automaticAdd" value="true"/>
				<booleanAttribute key="automaticValidate" value="false"/>
				<stringAttribute key="bootstrap" value=""/>
				<stringAttribute key="checked" value="[NONE]"/>
				<booleanAttribute key="clearConfig" value="false"/>
				<booleanAttribute key="clearws" value="false"/>
				<booleanAttribute key="clearwslog" value="false"/>
				<stringAttribute key="configLocation" value="${workspace_loc}/.metadata/.plugins/org.eclipse.pde.core/GraphitiEditor-Mac"/>
				<booleanAttribute key="default" value="true"/>
				<booleanAttribute key="includeOptional" value="true"/>
				<stringAttribute key="location" value="${workspace_loc}/../runtime-graphiti"/>
				<listAttribute key="org.eclipse.debug.ui.favoriteGroups">
				<listEntry value="org.eclipse.debug.ui.launchGroup.debug"/>
				<listEntry value="org.eclipse.debug.ui.launchGroup.run"/>
				</listAttribute>
				<booleanAttribute key="org.eclipse.jdt.launching.ATTR_USE_START_ON_FIRST_THREAD" value="true"/>
				<stringAttribute key="org.eclipse.jdt.launching.JRE_CONTAINER" value="org.eclipse.jdt.launching.JRE_CONTAINER/org.eclipse.jdt.internal.debug.ui.launcher.StandardVMType/JavaSE-1.7"/>
				<stringAttribute key="org.eclipse.jdt.launching.PROGRAM_ARGUMENTS" value="-os ${target.os} -ws ${target.ws} -arch ${target.arch} -nl ${target.nl} -consoleLog"/>
				<stringAttribute key="org.eclipse.jdt.launching.SOURCE_PATH_PROVIDER" value="org.eclipse.pde.ui.workbenchClasspathProvider"/>
				<stringAttribute key="org.eclipse.jdt.launching.VM_ARGUMENTS" value="-Dosgi.requiredJavaVersion=1.6 -XstartOnFirstThread -Dorg.eclipse.swt.internal.carbon.smallFonts -XX:MaxPermSize=256m -Xms40m -Xmx512m -Xdock:icon=../Resources/Eclipse.icns -XstartOnFirstThread -Dorg.eclipse.swt.internal.carbon.smallFonts"/>
				<booleanAttribute key="pde.generated.config" value="false"/>
				<stringAttribute key="pde.version" value="3.3"/>
				<stringAttribute key="product" value="org.eclipse.platform.ide"/>
				<booleanAttribute key="show_selected_only" value="false"/>
				<booleanAttribute key="tracing" value="false"/>
				<booleanAttribute key="useCustomFeatures" value="false"/>
				<booleanAttribute key="useDefaultConfig" value="true"/>
				<booleanAttribute key="useDefaultConfigArea" value="true"/>
				<booleanAttribute key="useProduct" value="true"/>
				<booleanAttribute key="usefeatures" value="false"/>
			</launchConfiguration>
    	'''
       fsa.generateFile("/GraphitiEditor-Mac.launch", pi.projectName, content);
    }
    
    def generateGraphitiEditorLaunchWin(SprayProjectInfo pi, IFileSystemAccess fsa) {
    	val content = '''
			<?xml version="1.0" encoding="UTF-8" standalone="no"?>
			<launchConfiguration type="org.eclipse.pde.ui.RuntimeWorkbench">
				<booleanAttribute key="append.args" value="true"/>
				<booleanAttribute key="askclear" value="true"/>
				<booleanAttribute key="automaticAdd" value="true"/>
				<booleanAttribute key="automaticValidate" value="false"/>
				<stringAttribute key="bootstrap" value=""/>
				<stringAttribute key="checked" value="[NONE]"/>
				<booleanAttribute key="clearConfig" value="false"/>
				<booleanAttribute key="clearws" value="false"/>
				<booleanAttribute key="clearwslog" value="false"/>
				<stringAttribute key="configLocation" value="${workspace_loc}/.metadata/.plugins/org.eclipse.pde.core/GraphitiEditor-Win"/>
				<booleanAttribute key="default" value="true"/>
				<booleanAttribute key="includeOptional" value="true"/>
				<stringAttribute key="location" value="${workspace_loc}/../runtime-graphiti"/>
				<listAttribute key="org.eclipse.debug.ui.favoriteGroups">
				<listEntry value="org.eclipse.debug.ui.launchGroup.debug"/>
				<listEntry value="org.eclipse.debug.ui.launchGroup.run"/>
				</listAttribute>
				<stringAttribute key="org.eclipse.jdt.launching.JRE_CONTAINER" value="org.eclipse.jdt.launching.JRE_CONTAINER/org.eclipse.jdt.internal.debug.ui.launcher.StandardVMType/JavaSE-1.7"/>
				<stringAttribute key="org.eclipse.jdt.launching.PROGRAM_ARGUMENTS" value="-os ${target.os} -ws ${target.ws} -arch ${target.arch} -nl ${target.nl} -consoleLog"/>
				<stringAttribute key="org.eclipse.jdt.launching.SOURCE_PATH_PROVIDER" value="org.eclipse.pde.ui.workbenchClasspathProvider"/>
				<stringAttribute key="org.eclipse.jdt.launching.VM_ARGUMENTS" value="-Dosgi.requiredJavaVersion=1.6 -Xms40m -Xmx512m"/>
				<booleanAttribute key="pde.generated.config" value="false"/>
				<stringAttribute key="pde.version" value="3.3"/>
				<stringAttribute key="product" value="org.eclipse.platform.ide"/>
				<booleanAttribute key="show_selected_only" value="false"/>
				<booleanAttribute key="tracing" value="false"/>
				<booleanAttribute key="useCustomFeatures" value="false"/>
				<booleanAttribute key="useDefaultConfig" value="true"/>
				<booleanAttribute key="useDefaultConfigArea" value="true"/>
				<booleanAttribute key="useProduct" value="true"/>
				<booleanAttribute key="usefeatures" value="false"/>
			</launchConfiguration>
    	'''
       fsa.generateFile("/GraphitiEditor-Win.launch", pi.projectName, content);
    }
}
