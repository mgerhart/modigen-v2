package org.eclipselabs.spray.xtext.ui.syntaxcoloring;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.RGB;
import org.eclipse.xtext.ui.editor.syntaxcoloring.DefaultHighlightingConfiguration;
import org.eclipse.xtext.ui.editor.syntaxcoloring.IHighlightingConfigurationAcceptor;
import org.eclipse.xtext.ui.editor.utils.TextStyle;

public class SprayHighlightingConfiguration extends DefaultHighlightingConfiguration {

	private static final RGB RGB_BLUE = new RGB(0, 0, 128);
	private static final RGB RGB_CADETBLUE = new RGB(55, 177, 183);
	private static final RGB RGB_DEEPSKYBLUE = new RGB(0, 104, 139);
	private static final RGB RGB_DARKGREEN = new RGB(46, 139, 87);
	private static final RGB RGB_RED = new RGB(255, 0, 0);
	
	public static final String DIAGRAM_ID = "diagram";
	public static final String PARAMETER_ID = "parameter";
	public static final String ACTION_ID = "actiongroup";
	public static final String METACLASS_ID = "metaclass";
	public static final String ATTRIBUTE_ID = "attribute";
	public static final String JAVA_ID = "java";
	
	
	@Override
	public void configure(IHighlightingConfigurationAcceptor acceptor) {
		super.configure(acceptor);
		
		acceptor.acceptDefaultHighlighting(DIAGRAM_ID, "Diagram", diagramTextStyle());
		acceptor.acceptDefaultHighlighting(PARAMETER_ID, "Parameter", parameterTextStyle());
		acceptor.acceptDefaultHighlighting(ACTION_ID, "Action Group", actionGroupTextStyle());
		acceptor.acceptDefaultHighlighting(METACLASS_ID, "Meta Class", metaClassStyle());
		acceptor.acceptDefaultHighlighting(ATTRIBUTE_ID, "Attribute", attributeStyle());
		acceptor.acceptDefaultHighlighting(JAVA_ID, "Java Class Reference", javaStyle());
	}
	
	public TextStyle diagramTextStyle() {
		TextStyle style = new TextStyle();
		style.setColor(RGB_BLUE);
		style.setStyle(SWT.BOLD);
		return style;
	}
	
	public TextStyle parameterTextStyle(){
		TextStyle style = new TextStyle();
		style.setColor(RGB_DARKGREEN);
		return style;
	}
	
	public TextStyle actionGroupTextStyle(){
		TextStyle style = new TextStyle();
		style.setColor(RGB_DEEPSKYBLUE);
		style.setStyle(SWT.BOLD);
		return style;
	}
	
	public TextStyle metaClassStyle(){
		TextStyle style = new TextStyle();
		style.setColor(RGB_DARKGREEN);
		style.setStyle(SWT.BOLD);
		return style;
	}
	
	public TextStyle attributeStyle(){
		TextStyle style = new TextStyle();
		style.setColor(RGB_DARKGREEN);
		return style;
	}
	
	public TextStyle javaStyle(){
		TextStyle style = new TextStyle();
		style.setColor(RGB_RED);
		style.setStyle(SWT.BOLD);
		return style;
	}
}
