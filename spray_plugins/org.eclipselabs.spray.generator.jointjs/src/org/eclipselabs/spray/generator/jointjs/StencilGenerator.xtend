package org.eclipselabs.spray.generator.jointjs

import java.util.ArrayList
import java.util.HashMap
import java.util.List
import java.util.Map
import java.util.NoSuchElementException
import org.eclipselabs.spray.Diagram
import org.eclipselabs.spray.Node
import org.eclipselabs.spray.styles.Style
import org.apache.commons.logging.Log
import org.apache.commons.logging.LogFactory
import org.eclipse.emf.ecore.resource.Resource
import org.eclipselabs.spray.shapes.ShapeDefinitionimport org.eclipse.xtext.xbase.XStringLiteral

class StencilGenerator {
	
	private static Log   LOGGER       = LogFactory::getLog("StencilGenerator");
	private var packageName = ""
	
	def generate(Diagram diagram){
		'''
		«generateHeader»
		 
		«generateStencilGroups(diagram)»
		 
		«generateShapes(diagram)»
		«generateGroupsToStencilMapping(getNodeToPaletteMapping(diagram))»
		 
		«generateDocumentReadyFunction(diagram)»
		''' 
	}
	
	def generateHeader(){
		'''
		/*
		* This is a generated stencil file for JointJS
		*/
		'''
	}
	
	def generateStencilGroups(Diagram diagram){
		'''
		Stencil.groups = {
		«var i = 0»
		«val groupSet = getNodeToPaletteMapping(diagram).keySet()»
		«FOR group : groupSet»
			«getVarName(group)»: {index: «i=i+1», label: '«group»' }«IF group != groupSet.last»,«ENDIF»
		«ENDFOR»
		};
		'''
	}
	
	def generateShapes(Diagram diagram){
		'''
		«FOR node: diagram.nodes»
		var «getVarName(node.name)» = new joint.shapes.«packageName».«getClassName(getShapeName(node))»({
			«IF node.onCreate != null && node.onCreate.askFor != null»
			ecoreAttributes: [
				{
					ecore: '«node.onCreate.askFor.name»',
					cellPath: ['attrs', '.label', 'text']
				}
			],
			«ENDIF»
			ecoreName: '«node.type.name»'
		});
		
		
		«ENDFOR»
		'''
	}
	
	def generateGroupsToStencilMapping(Map<String,List<Node>> mapping){
		'''
		Stencil.shapes = {
			«val entrySet = mapping.entrySet()»
			«FOR group : entrySet»
			«generateShapesToGroupMapping(group.key, group.value, group == entrySet.last)»
			«ENDFOR»
		};
		'''
	}
	
	def generateShapesToGroupMapping(String group, List<Node> nodes, boolean isLast){
		'''
		«getVarName(group)»: [
			«FOR node: nodes»
			«getVarName(node.name)»«IF node != nodes.last»,«ENDIF»
			«ENDFOR»
		]«IF !isLast»,«ENDIF»
		'''
	}
	
	def generateDocumentReadyFunction(Diagram diagram){
		'''
		$(document).ready(function() {
			«FOR node : diagram.nodes»
				//Get Style of Diagram
				«getVarName(node.name)».attr(getStyle("«diagram.style.dslStyle.name»"));
				«IF getStyleForNode(node, diagram.style.dslStyle) != null»
				//Get Style of Element
				«getVarName(node.name)».attr(getStyle("«getStyleForNode(node, diagram.style.dslStyle)»"));
				«ENDIF»
				//Get Style of Shape and Inline Style
				«getVarName(node.name)».attr(getShapeStyle("«getClassName(getShapeName(node))»"));
				
				//Fill in text fields
				«FOR property : node.shape.properties.filter[s | s.value != null && s.value instanceof XStringLiteral]»
					«getVarName(node.name)».attr({'.«property.key.dslKey»':{text: '«(property.value as XStringLiteral).value»'}});
				«ENDFOR»
				
			«ENDFOR»
			«IF diagram.style.dslStyle.name != null»
				var style = document.createElement('style');
				style.id = 'highlighting-style';
				style.type = 'text/css';
				style.innerHTML = getDiagramHighlighting("«diagram.style.dslStyle.name»");
				document.getElementsByTagName('head')[0].appendChild(style);
			«ENDIF»
		});
		'''
	}
	
	def setPackageName(String packageName){
		this.packageName = packageName
	}
	
	private def getNodeToPaletteMapping(Diagram diagram){
		var mapping = new HashMap<String,List<Node>>
		for(node : diagram.nodes){
			var paletteName = node.palette.paletteCompartment
			if(mapping.containsKey(paletteName)){
				mapping.get(paletteName).add(node)
			}else{
				var list = new ArrayList()
				list.add(node)
				mapping.put(paletteName, list)
			}
		}
		mapping
	}
	
	private def getVarName(String name){
		name.replaceAll("\\W", "").toFirstLower
	}
	
	private def getClassName(String name){
		name.replaceAll("\\W", "").toFirstUpper
	}
	
	private def getShapeName(Node node){
		var shapeReference = node.shape.shape
		if(shapeReference.dslShape != null){
			shapeReference.dslShape.name
		}else if(shapeReference.jvmShape != null){
			shapeReference.jvmShape.simpleName
		}else{
			throw new NoSuchElementException("No Shape defined for node "+node.name)
		}
	}
	
	private def getStyleForNode(Node node, Style defaultStyle){
		val shape = getShape(node);
		if(shape.style != null){
			shape.style.dslStyle.name
		}else{
			null
		}
	}
	
	private def getShape(Node node){
		var shapeReference = node.shape.shape;
		if(shapeReference.dslShape != null){
			shapeReference.dslShape
		}else if(shapeReference.jvmShape != null){
			throw new IllegalArgumentException("Java Shapes are not allowed for JointJS Generation")
		}else{
			throw new NoSuchElementException("No Shape defined for node "+node.name)
		}
	}
	
}