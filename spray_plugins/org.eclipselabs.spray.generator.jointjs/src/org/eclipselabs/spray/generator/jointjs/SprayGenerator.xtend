/** ****************************************************************************
 * Copyright (c)  The Spray Project.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Spray Dev Team - initial API and implementation
 **************************************************************************** */
package org.eclipselabs.spray.generator.jointjs

import javax.inject.Inject
import com.google.inject.Provider
import org.apache.commons.logging.Log
import org.apache.commons.logging.LogFactory
import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.xtext.generator.IFileSystemAccess
import org.eclipse.xtext.generator.IGenerator
import org.eclipselabs.spray.common.generator.ProjectProperties
import org.eclipselabs.spray.xtext.generator.filesystem.JavaGenFile
import org.eclipse.xtext.xbase.compiler.JvmModelGenerator
import org.eclipselabs.spray.Diagram
import org.eclipse.xtext.generator.AbstractFileSystemAccess
import org.eclipse.xtext.generator.OutputConfiguration

class SprayGenerator extends JvmModelGenerator implements IGenerator {
    @Inject Provider<JavaGenFile> genFileProvider
	@Inject extension StencilGenerator stencilGenerator
	@Inject extension ValidatorGenerator validatorGenerator
	@Inject extension BuildZipGenerator buildZipGenerator
	@Inject extension ReadmeGenerator readmeGenerator
	@Inject extension LinkhelperGenerator linkhelperGenerator
    
    private static Log   LOGGER       = LogFactory::getLog("SprayGenerator");
    private static String JOINTJS_STENCIL_FILENAME		= "stencil.js"
    private static String JOINTJS_VALIDATOR_FILENAME	= "validator.js"
    private static String JOINTJS_PATH 					= "jointjs-gen/diagram"
    private static String JOINTJS_PATH_ROOT				= "jointjs-gen"
    private static String JOINTJS_ZIP_ANT_FILENAME		= "buildZip.xml"
    private static String JOINTJS_README_FILENAME		= "Readme.xml"
    private static String JOINTJS_LINKHELPER_FILENAME	= "linkhelper.js"
    
    /**
     * This method is a long sequence of calling all templates for the code generation
     */
    override void doGenerate(Resource resource, IFileSystemAccess fsa) {  
       
        if (!resource.URI.lastSegment().endsWith(ProjectProperties::SPRAY_FILE_EXTENSION)) {
            LOGGER.info("Spray generator is NOT producing JointJS code for model " + resource.URI)
            return;
        }
        
        LOGGER.info("Spray generator is producing JointJS code for model " + resource.URI)
        ProjectProperties::setModelUri(resource.URI)
		super.doGenerate(resource, fsa);
		
		val packageName = resource.URI.lastSegment.replace(".spray", "")
		stencilGenerator.setPackageName(packageName)
		
		val JavaGenFile java = genFileProvider.get()
        val jointJSSprayOutputConfName = "JointJSSprayOutputConfiguration"
        val jointJSSprayRootOutputConfName = "JointJSSprayRootOutputConfiguration"
        
        java.access = fsa
        
        fsa.addJointJSOutputConfiguration(jointJSSprayOutputConfName)
        var diagram = resource.allContents.filter(typeof(Diagram)).head
        fsa.generateFile(JOINTJS_STENCIL_FILENAME, jointJSSprayOutputConfName, stencilGenerator.generate(diagram).toString())
        fsa.generateFile(JOINTJS_VALIDATOR_FILENAME, jointJSSprayOutputConfName, validatorGenerator.generate(diagram).toString())
        fsa.generateFile(JOINTJS_README_FILENAME, jointJSSprayOutputConfName, readmeGenerator.generate(diagram).toString())
        fsa.generateFile(JOINTJS_LINKHELPER_FILENAME, jointJSSprayOutputConfName, linkhelperGenerator.generate(diagram).toString)

        fsa.addJointJSOutputRootConfiguration(jointJSSprayRootOutputConfName)
        fsa.generateFile(JOINTJS_ZIP_ANT_FILENAME, jointJSSprayRootOutputConfName, buildZipGenerator.generate().toString())
    }

   def addJointJSOutputRootConfiguration(IFileSystemAccess fsa, String name){
   	fsa.addOutputConfiguration(name, JOINTJS_PATH_ROOT)
   }

   def addJointJSOutputConfiguration(IFileSystemAccess fsa, String name){
   	fsa.addOutputConfiguration(name, JOINTJS_PATH)
   }
   
   def addOutputConfiguration(IFileSystemAccess fsa, String name, String path){
   	if(fsa instanceof AbstractFileSystemAccess) {
   			val aFsa = fsa as AbstractFileSystemAccess
   			if(!aFsa.outputConfigurations.containsKey(name)) {
	   			val outputConfigurations = <String, OutputConfiguration> newHashMap
	   			outputConfigurations.putAll(aFsa.outputConfigurations)
	   			val imageOutputConfiguration = new OutputConfiguration(name)
	   			imageOutputConfiguration.outputDirectory = path
	   			imageOutputConfiguration.createOutputDirectory = true
	   			imageOutputConfiguration.overrideExistingResources = true
	   			imageOutputConfiguration.setDerivedProperty = true
	   			outputConfigurations.put(name, imageOutputConfiguration) 
	   			aFsa.setOutputConfigurations(outputConfigurations)
   			}
   		}
   }
}
