package org.eclipselabs.spray.generator.jointjs

import org.eclipselabs.spray.Diagram
import org.eclipse.xtext.xbase.XStringLiteral

class LinkhelperGenerator {
	def generate(Diagram diagram){
		'''
		«generateHead»
		«generateLinkhelper(diagram)»
		'''
	}
	
	def generateHead() {
		'''
		/*
		* This is a generated linkhelper file for JointJS
		*/
		'''
	}
	
	protected def generateLinkhelper(Diagram diagram){
		'''
		var linkhelper = {
			«generatePlacingTexts(diagram)»
			«generateHelperMethods»
		};
		'''
	}
	
	protected def generatePlacingTexts(Diagram diagram){
		'''
		placingTexts:{
			«FOR e : diagram.edges»
			«e.name»: {
			«val stringProperties = e.connection.connectionProperties.filter[p | p.value != null && p.value instanceof XStringLiteral]»
				«FOR property : stringProperties»
				'«property.key.dslKey»': "«(property.value as XStringLiteral).getValue()»"«IF property != stringProperties.last»,«ENDIF»
				«ENDFOR»
			}«IF e != diagram.edges.last»,«ENDIF»
			«ENDFOR»
		},
		
		'''
	}
	
	protected def generateHelperMethods(){
		'''
		getLabelText: function(edge, textId){
			var text = this.placingTexts[edge][textId];
			if(text === undefined){
				text = "";
			}

			return text;
		}
		'''
	}
}