package org.eclipselabs.spray.generator.jointjs

import org.eclipselabs.spray.Diagram
import java.sql.Timestamp

class ReadmeGenerator {
	
	java.util.Date date = new java.util.Date();
	def generate(Diagram diagram) {
		'''
		<project>
			<projectInformation>
				<name>SprayOnline</name>
				<created>«new Timestamp(date.getTime())»</created>
		
			</projectInformation>
			<diagram>
				<name>«diagram.name»</name>
			</diagram>
		</project>
		'''
	}
}