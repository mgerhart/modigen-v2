package org.eclipselabs.spray.generator.jointjs

import org.eclipselabs.spray.Diagram
import java.util.HashMap
import org.eclipselabs.spray.Node
import org.eclipse.emf.common.util.EList
import org.eclipselabs.spray.Edge
import java.util.Map
import java.util.List
import java.util.ArrayList
import java.util.NoSuchElementException
import org.eclipselabs.spray.styles.Styleimport org.eclipse.emf.ecore.EClass

class ValidatorGenerator {
	
	def generate(Diagram diagram) {
		'''
		«generateHead()»
		var validator = {
			«generateTargetMatrix(diagram)»
		 	
			«generateSourceMatrix(diagram)»
			
			«generateEdgeData(diagram)»
			
			«generateCompartmentMatrix(diagram)»
			
			«generateAccessMethods()»
		};
		'''
	}
	
	def generateHead() {
		'''
		/*
		* This is a generated validator file for JointJS
		*/
		'''
	}
	
	def generateTargetMatrix(Diagram diagram){
		'''
		targetMatrix: {
		«var targetMatrix = getTargetMatrix(diagram)»
		«val clazzes = targetMatrix.entrySet()»
			«FOR clazz : clazzes»
			«clazz.key.name»: {
				«generateEdgeMap(clazz.value)»
			}«IF clazz != clazzes.last»,«ENDIF»
			«ENDFOR»
		},
		'''
	}
	
	def generateEdgeMap(Map<String, Boolean> edgeMap){
		'''
		«val edges = edgeMap.entrySet»
		«FOR edge : edges»
			«edge.key»: «IF edge.value»true«ELSE»false«ENDIF»«IF edge != edges.last»,«ENDIF»		
		«ENDFOR»
		'''
	}
	
	def generateSourceMatrix(Diagram diagram){
		'''
		sourceMatrix: {
		«val sourceMatrix = getSourceMatrix(diagram)»
		«val clazzes = sourceMatrix.entrySet»
			«FOR clazz : clazzes»
			«clazz.key.name»: {
				«generateEdgeMap(clazz.value)»
			}«IF clazz != clazzes.last»,«ENDIF»
			«ENDFOR»
		},
		'''
	}
	
	def generateAccessMethods(){
		'''
		isValidTarget: function(nodeName, edgeName){
			return this.targetMatrix[nodeName][edgeName];
		},
		
		isValidSource: function(nodeName, edgeName){
			return this.sourceMatrix[nodeName][edgeName];
		},
		
		getEdgeData: function(edgeName){
			return this.edgeData[edgeName];
		},
		
		getValidEdges: function(sourceName, targetName){
			var validEdges = [];
			var candidateEdges = Object.keys(this.sourceMatrix[sourceName]);
			for(var i=0; i < candidateEdges.length; i++){
				if(this.isValidSource(sourceName, candidateEdges[i]) && this.isValidTarget(targetName, candidateEdges[i])){
					validEdges.push(candidateEdges[i]);
				}
			}
			
			return validEdges;
		},
		
		getValidCompartments: function(childName, parentName){
			return this.compartmentMatrix[childName][parentName];
		},
		
		isValidChild: function(childName, parentName){
			return this.getValidCompartments(childName, parentName).length > 0;
		}
		'''
	}
	
	def generateEdgeData(Diagram diagram){
		'''
		edgeData: {
			«FOR edge : diagram.edges»
			«edge.name»: {
				type: "«edge.type.name»",
				from: "«edge.from.name»",
				to: "«edge.to.name»",
				style: "«getStyleForEdge(edge, diagram.style.dslStyle)»"
			}«IF edge != diagram.edges.last»,«ENDIF»
			«ENDFOR»
		},
		'''
	}
	
	def generateCompartmentMatrix(Diagram diagram){
		'''
		compartmentMatrix: {
			«val compartmentMatrix = getCompartmentMatrix(diagram.nodes).entrySet»
			«FOR entry : compartmentMatrix»
			«entry.key.name»: {
				«generateCompartmentMap(entry.value)»
			}«IF entry != compartmentMatrix.last», «ENDIF»
			«ENDFOR»
		},
		'''
	}
	
	def generateCompartmentMap(Map<String, List<String>> compartmentMap){
		'''
		«val compartmentData = compartmentMap.entrySet»
		«FOR node : compartmentData»
			«node.key»: [«FOR compartment : node.value»"«compartment»"«IF compartment != node.value.last», «ENDIF»«ENDFOR»]«IF node!=compartmentData.last», «ENDIF»
		«ENDFOR»
		'''
	}
	
	private def getTargetMatrix(Diagram diagram){
		var targetMatrix = new HashMap<EClass, HashMap<String, Boolean>>
		for(node : diagram.nodes){
			var edgeTargetMap = getEdgeTargetMap(node, diagram.edges)
			targetMatrix.put(node.type, edgeTargetMap)
		}
		targetMatrix
	}
	
	private def getSourceMatrix(Diagram diagram){
		var sourceMatrix = new HashMap<EClass, HashMap<String, Boolean>>
		for(node: diagram.nodes){
			var edgeSourceMap = getEdgeSourceMap(node, diagram.edges)
			sourceMatrix.put(node.type, edgeSourceMap)
		}
		sourceMatrix
	}
	
	private def getCompartmentMatrix(EList<Node> nodes){
		var compartmentMatrix = new HashMap<Node, HashMap<String, List<String>>>
		for(node: nodes){
			var compartmentMap = getCompartmentMap(node, nodes)
			compartmentMatrix.put(node, compartmentMap)
		}
		compartmentMatrix
	}
	
	private def getEdgeTargetMap(Node node, EList<Edge> edges){
		var edgeTargetMap = new HashMap<String, Boolean>
		val nodeClass = node.type
		for(edge : edges){
			val targetClass = edge.to.EReferenceType
			edgeTargetMap.put(edge.name, targetClass.isSuperTypeOf(nodeClass))
		}
		edgeTargetMap
	}
	
	private def getEdgeSourceMap(Node node, EList<Edge> edges){
		var edgeSourceMap = new HashMap<String, Boolean>
		val nodeClass = node.type
		for(edge : edges){
			val sourceClass = edge.from.EReferenceType
			edgeSourceMap.put(edge.name, sourceClass.isSuperTypeOf(nodeClass))
		}
		edgeSourceMap
	}
	
	private def getCompartmentMap(Node node, EList<Node> nodeList){
		var compartmentMap = new HashMap<String, List<String>>
		val nodeClass = node.type
		for(parent : nodeList){
			var validCompartments = new ArrayList<String>
			for(compartment : parent.shape.shapeCompartments){
				if(compartment.nestedShape.EReferenceType.isSuperTypeOf(nodeClass)){
					validCompartments.add(compartment.nestedShape.name)
				}
			}
			compartmentMap.put(parent.name, validCompartments)
		}
		compartmentMap
	}
	
	private def getStyleForEdge(Edge edge, Style defaultStyle){
		val connection = getConnection(edge)
		connection.name
	}
	
	private def getConnection(Edge edge){
		val connectionReference = edge.connection.connection
		if(connectionReference.dslConnection != null){
			connectionReference.dslConnection
		}else if(connectionReference.jvmConnection != null){
			throw new IllegalArgumentException("Java Connections are not allowed for JointJS Generation")
		}else{
			throw new NoSuchElementException("No connection defined for edge "+edge.name)
		}
	}
}