package org.eclipselabs.spray.xtext.validation;

import javax.inject.Inject;

import java.util.HashSet;
import java.util.Set;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.common.types.JvmType;
import org.eclipse.xtext.common.types.access.IJvmTypeProvider;
import org.eclipse.xtext.validation.Check;
import org.eclipselabs.spray.*;
import org.eclipselabs.spray.xtext.scoping.PackageSelector;
import org.eclipselabs.spray.xtext.util.GenModelHelper;
import org.eclipselabs.spray.xtext.util.TextBodyFetcher;

import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;

/**
 * Custom validation rules. 
 *
 * see http://www.eclipse.org/Xtext/documentation.html#validation
 */
public class SprayJavaValidator extends AbstractSprayJavaValidator implements IssueCodes {
	
	@Inject
    private GenModelHelper           genModelHelper;
	@Inject
    private IJvmTypeProvider.Factory typeProviderFactory;
	@Inject
    private PackageSelector          packageSelector;
	@Inject
    private TextBodyFetcher          textBodyFetcher;
	
	@Check
    public void checkGenModelAvailable(MetaClass clazz) {
        EClass type = clazz.getType();
        try {
            // call of getEPackageClassName will result in an IllegalStateException when no GenModel was found
            if (type.eIsProxy()) {
                return; // scoping problem; will be reported as separate problem
            }
            genModelHelper.getEPackageClassName(type);
        } catch (IllegalStateException e) {
            error("No genmodel registered for EClass " + type.getName(), clazz, SprayPackage.Literals.META_CLASS__TYPE);
        }
    }
	
	@Check
    public void checkImports(final Import imp) {
        if (imp.getImportedNamespace().endsWith(".*")) {
            Iterable<EPackage> ePackages = packageSelector.getFilteredEPackages(imp);
            for (EPackage ePackage : ePackages) {
                if ((ePackage.getName() + ".*").equals(imp.getImportedNamespace())) {
                    return;
                }
            }
            error("The import " + imp.getImportedNamespace() + " cannot be resolved", SprayPackage.Literals.IMPORT__IMPORTED_NAMESPACE);
        }
        IJvmTypeProvider typeProvider = typeProviderFactory.findOrCreateTypeProvider(imp.eResource().getResourceSet());
        JvmType jvmType = typeProvider.findTypeByName(imp.getImportedNamespace());
        if (jvmType == null) {
            error("The import " + imp.getImportedNamespace() + " cannot be resolved", SprayPackage.Literals.IMPORT__IMPORTED_NAMESPACE);
        }
    }
	
	@Check
	void checkShapeReference(final Node node) {
		if (node.getShape() == null) {
			error("shape reference not specified", SprayPackage.Literals.NODE__SHAPE);
		}
	}
	
	@Check
	void checkPallete(final MetaClass clazz) {
		if (clazz.getPalette() == null) {
			error("palette not specified", SprayPackage.Literals.META_CLASS__PALETTE);
		}
	}
	
	@Check
    public void checkEdgeFromToReference(final Edge edge) {
		if (edge.getFrom() == null) {
            error("from reference not specified", SprayPackage.Literals.EDGE__FROM);
        }
		if (edge.getTo() == null) {
            error("to reference not specified", SprayPackage.Literals.EDGE__TO);
        }
    }
	
	@Check
	public void checkConnection(final Edge edge) {
		if (edge.getConnection() == null) {
			error("connection not specified", SprayPackage.Literals.EDGE__CONNECTION);
		}
	}
	
	@Check
    public void checkIdReferences(final ShapeProperty shapePropertyAssignment) {
        ShapeDslKey shapeDslKey = shapePropertyAssignment.getKey();
        if (shapeDslKey != null) {
            String currentKey = shapeDslKey.getDslKey();
            if (currentKey != null) {
                final Predicate<EObject> filterPredicate = textBodyFetcher.getPropertyAssignmentIdsFilter();
                Set<String> textBodyIds = textBodyFetcher.getTextBodyIds(shapePropertyAssignment, textBodyFetcher.getShapeContainerElementResolver(), filterPredicate);
                if (!textBodyIds.contains(currentKey)) {
                    error("The given id " + currentKey + " cannot be resolved inside the referenced shape", SprayPackage.Literals.SHAPE_PROPERTY__KEY);
                }
            }
        }
    }
	
	@Check
	public void checkIdReferences(final ShapeCompartment shapeCompartmentAssignment){
		ShapeDslKey shapeDslKey = shapeCompartmentAssignment.getCompartment();
        if (shapeDslKey != null) {
            String currentKey = shapeDslKey.getDslKey();
            if (currentKey != null) {
                final Predicate<EObject> filterPredicate = textBodyFetcher.getCompartmentAssignmentIdsFilter();
                Set<String> textBodyIds = textBodyFetcher.getTextBodyIds(shapeCompartmentAssignment, textBodyFetcher.getShapeContainerElementResolver(), filterPredicate);
                if (!textBodyIds.contains(currentKey)) {
                    error("The given id " + currentKey + " cannot be resolved inside the referenced shape", SprayPackage.Literals.SHAPE_COMPARTMENT__COMPARTMENT);
                }
            }
        }
    }
	
	@Check
	public void checkDuplicateActionNames(final Action action) {
		if (action != null) {
			
			final Predicate<Action> filterActions = new Predicate<Action>() {
				@Override
				public boolean apply(Action input) {
					return action.getName().equals(input.getName());
				}
			};
			
			ActionGroup actionGroup = action.getActionGroup();
			
			if (actionGroup != null && actionGroup.eContainer() != null) {
				
				Diagram diagram = null;
				
				// local actionGroup
				if(actionGroup.eContainer() instanceof Node) {
					Node node = (Node)actionGroup.eContainer();
					diagram = node.getDiagram();
				}
				
				else if(actionGroup.eContainer() instanceof Edge) {
					Edge edge = (Edge)actionGroup.eContainer();
					diagram = edge.getDiagram();
				}
				
				// global actionGroup
				else if(actionGroup instanceof GlobalActionGroup) {
					diagram = (Diagram)actionGroup.eContainer();
				}
				
				if (diagram != null) {
					Set<Action> actions = new HashSet<Action>();
					
					for (Node n : diagram.getNodes()) {
						if(n.getActionGroup() != null && n.getActionGroup().getActions() != null) {
							actions.addAll(n.getActionGroup().getActions());
						}
					}
					
					for (Edge e : diagram.getEdges()) {
						if (e.getActionGroup() != null && e.getActionGroup().getActions() != null) {
							actions.addAll(e.getActionGroup().getActions());
						}
					}
					
					for (GlobalActionGroup ag : diagram.getGlobalActionGroups()) {
						if (ag.getActions() != null) {
							actions.addAll(ag.getActions());
						}
					}
					
					if (action != Iterables.find(actions, filterActions)) {
						error ("Duplicate action name " + action.getName(), SprayPackage.Literals.NAMED_ELEMENT__NAME);
					}
				}
			}
		}
	}
}
