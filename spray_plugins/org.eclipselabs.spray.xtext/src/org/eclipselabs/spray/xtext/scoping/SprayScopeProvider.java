package org.eclipselabs.spray.xtext.scoping;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.xtext.EcoreUtil2;
import org.eclipse.xtext.common.types.JvmEnumerationLiteral;
import org.eclipse.xtext.common.types.JvmEnumerationType;
import org.eclipse.xtext.common.types.JvmGenericType;
import org.eclipse.xtext.common.types.JvmMember;
import org.eclipse.xtext.common.types.JvmType;
import org.eclipse.xtext.common.types.TypesPackage;
import org.eclipse.xtext.resource.EObjectDescription;
import org.eclipse.xtext.resource.IEObjectDescription;
import org.eclipse.xtext.scoping.IScope;
import org.eclipse.xtext.scoping.Scopes;
import org.eclipse.xtext.scoping.impl.FilteringScope;
import org.eclipse.xtext.scoping.impl.MapBasedScope;
import org.eclipse.xtext.xbase.scoping.XbaseScopeProvider;
import org.eclipselabs.spray.Action;
import org.eclipselabs.spray.Connection;
import org.eclipselabs.spray.Container;
import org.eclipselabs.spray.Diagram;
import org.eclipselabs.spray.Edge;
import org.eclipselabs.spray.MetaClass;
import org.eclipselabs.spray.Node;
import org.eclipselabs.spray.OnCreate;
import org.eclipselabs.spray.Shape;
import org.eclipselabs.spray.SprayStyleRef;
import org.eclipselabs.spray.shapes.xtext.scoping.ConnectionScopeRestrictor;
import org.eclipselabs.spray.shapes.xtext.scoping.ShapeScopeRestrictor;
import org.eclipselabs.spray.styles.xtext.scoping.StyleScopeRestrictor;

import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;

import static org.eclipselabs.spray.SprayPackage.Literals.SHAPE_PROPERTY__ATTRIBUTE;
import static org.eclipselabs.spray.SprayPackage.Literals.ON_CREATE__ASK_FOR;
import static org.eclipselabs.spray.SprayPackage.Literals.CONTAINER__CONTAINMENT_REFERENCE;
import static org.eclipselabs.spray.SprayPackage.Literals.EDGE__FROM;
import static org.eclipselabs.spray.SprayPackage.Literals.EDGE__TO;
import static org.eclipselabs.spray.SprayPackage.Literals.SHAPE_DSL_KEY__JVM_KEY;
import static org.eclipselabs.spray.SprayPackage.Literals.SHAPE_COMPARTMENT__NESTED_SHAPE;
import static org.eclipselabs.spray.SprayPackage.Literals.INPUT__INPUT;
import static org.eclipselabs.spray.SprayPackage.Literals.OUTPUT__OUTPUT;;


public class SprayScopeProvider extends XbaseScopeProvider {

	@Override
	public IScope getScope(EObject context, EReference reference){
		IScope scope = IScope.NULLSCOPE;
		if(reference == SHAPE_PROPERTY__ATTRIBUTE){
			return scope_ShapeProperty_attribute(context);
		}else if(reference == CONTAINER__CONTAINMENT_REFERENCE){
			return scope_Container_containmentReference(context);
		}else if(reference == ON_CREATE__ASK_FOR){
			return scope_OnCreate_askFor(context);
		}else if(reference == EDGE__FROM){
			return scope_Edge_from(context);
		}else if(reference == EDGE__TO){
			return scope_Edge_to(context);
		}else if(reference == TypesPackage.Literals.JVM_PARAMETERIZED_TYPE_REFERENCE__TYPE){
			return scope_JvmParametrizedTypeReference_type(context, reference);
		}else if(reference == SHAPE_DSL_KEY__JVM_KEY){
			return scope_ShapeDslKey_jvmKey(context);
		}else if(reference == SHAPE_COMPARTMENT__NESTED_SHAPE){
			return scope_ShapeCompartment_nestedShape(context);
		}else if(reference == INPUT__INPUT) {
			return scope_Node_Input(context);
		}else if(reference == OUTPUT__OUTPUT) {
			return scope_Node_Output(context);
		}
		else{
			return super.getScope(context, reference);
		}
	}
	
	protected IScope scope_Node_Input(EObject context) {
		final Node node = EcoreUtil2.getContainerOfType(context, Node.class);
		if(node == null || node.getType() == null || node.getType().getEAllReferences() == null){
			return IScope.NULLSCOPE;
		}
		Predicate<EReference> filter = new Predicate<EReference>(){
			public boolean apply(EReference input){
				return input != node.getInput() && !input.isDerived();
			}
		};
		Iterable<EReference> targetReferences = Iterables.filter(node.getType().getEAllReferences(), filter);
		return MapBasedScope.createScope(IScope.NULLSCOPE, Scopes.scopedElementsFor(targetReferences));		
	}
	
	protected IScope scope_Node_Output(EObject context) {
		final Node node = EcoreUtil2.getContainerOfType(context, Node.class);
		if(node == null || node.getType() == null || node.getType().getEAllReferences() == null){
			return IScope.NULLSCOPE;
		}
		Predicate<EReference> filter = new Predicate<EReference>(){
			public boolean apply(EReference input){
				return !input.isDerived();
			}
		};
		Iterable<EReference> targetReferences = Iterables.filter(node.getType().getEAllReferences(), filter);
		return MapBasedScope.createScope(IScope.NULLSCOPE, Scopes.scopedElementsFor(targetReferences));
	}
	
	protected IScope scope_Edge_to(EObject context){
		final Edge edge = (Edge) context;
		if(edge == null || edge.getType() == null || edge.getType().getEAllReferences() == null){
			return IScope.NULLSCOPE;
		}
		Predicate<EReference> filter = new Predicate<EReference>(){
			public boolean apply(EReference input){
				return input != edge.getFrom() && !input.isDerived();
			}
		};
		Iterable<EReference> targetReferences = Iterables.filter(edge.getType().getEAllReferences(), filter);
		return MapBasedScope.createScope(IScope.NULLSCOPE, Scopes.scopedElementsFor(targetReferences));
	}
	
	protected IScope scope_Edge_from(EObject context){
		final Edge edge = (Edge) context;
		if(edge == null || edge.getType() == null || edge.getType().getEAllReferences() == null){
			return IScope.NULLSCOPE;
		}
		Predicate<EReference> filter = new Predicate<EReference>(){
			public boolean apply(EReference input){
				return !input.isDerived();
			}
		};
		Iterable<EReference> targetReferences = Iterables.filter(edge.getType().getEAllReferences(), filter);
		return MapBasedScope.createScope(IScope.NULLSCOPE, Scopes.scopedElementsFor(targetReferences));
	}
	
	protected IScope scope_ShapeProperty_attribute(EObject context){
		MetaClass metaClass = EcoreUtil2.getContainerOfType(context, MetaClass.class);
		if(metaClass == null){
			return IScope.NULLSCOPE;
		}
		Predicate<EObject> filter = new Predicate<EObject>(){
			public boolean apply(EObject input){
				if(input instanceof EAttribute){
					if(((EAttribute) input).getEType().getName().equals("EString")){
						return true;
					}
				}
				return false;
			}
		};
		return Scopes.scopeFor(Iterables.filter(metaClass.getType().getEAllAttributes(), filter));
	}
	
	protected IScope scope_OnCreate_askFor(EObject context){
		MetaClass metaClass = ((OnCreate) context).getMetaClass();
		Container container = metaClass.getContainer();
		if(container == null){
			return IScope.NULLSCOPE;
		}
		
		EReference ref = container.getContainmentReference();
		if(ref == null){
			return IScope.NULLSCOPE;
		}else if(ref.eIsProxy()){
			ref = (EReference) EcoreUtil.resolve(ref, context);
			if(ref.eIsProxy()){
				return IScope.NULLSCOPE;
			}
		}
		Predicate<EAttribute> filter = new Predicate<EAttribute>(){
			public boolean apply(EAttribute input){
				return input.getEType() instanceof EDataType;
			}
		};
		Iterable<EAttribute> simpleAttributes = Iterables.filter(ref.getEReferenceType().getEAllAttributes(), filter);
		
		return MapBasedScope.createScope(IScope.NULLSCOPE, Scopes.scopedElementsFor(simpleAttributes));
	}
	
	protected IScope scope_Container_containmentReference(EObject context){
		Class<? extends MetaClass> classType;
		Diagram diagram;
		Node node = EcoreUtil2.getContainerOfType(context, Node.class);
		Edge edge = EcoreUtil2.getContainerOfType(context, Edge.class);
		final MetaClass metaClass = getMetaClassFromNodeAndEdge(node, edge);
		if(node != null){
			classType = Node.class;
			diagram = node.getDiagram();
		}else if(edge != null){
			classType = Edge.class;
			diagram = edge.getDiagram();
		}else{
			return IScope.NULLSCOPE;
		}
		
		EClass diagramModelType = diagram.getModelType();
		Predicate<EReference> filter = new Predicate<EReference>(){
			public boolean apply(EReference input){
				boolean superType = false;
				if(metaClass != null && metaClass.getType() != null){
					superType = isSuperType(input.getEReferenceType(), metaClass.getType());
				}
				return superType;
			}
		};
		// get all containments of EClass contained in this package
        List<EReference> containmentReferences = new ArrayList<EReference>();
        containmentReferences.addAll(diagramModelType.getEAllContainments());
        //if the MetaClass is an edge take also the containment dependencies of the source type
        if(classType == Edge.class && edge.getFrom() != null){
        	EClass sourceType = (EClass) edge.getFrom().getEType();
        	if(sourceType != null){
        		containmentReferences.addAll(sourceType.getEAllContainments());
        	}
        }
        return Scopes.scopeFor(Iterables.filter(containmentReferences, filter));
	}
	
	// Will cast Node to MetaClass, if node isn't null. Otherwise edge. If edge is also null, returns null
	private MetaClass getMetaClassFromNodeAndEdge(Node node, Edge edge){
		if(node != null){
			return (MetaClass) node;
		}else if(edge != null){
			return (MetaClass) edge;
		}else{
			return null;
		}
	}
	
	private boolean isSuperType(EClass eClass1, EClass eClass2) {
        return isSuperType(eClass1, eClass2, new ArrayList<EClass>());
    }
	
	private boolean isSuperType(EClass eClass1, EClass eClass2, List<EClass> visited) {
        boolean superType = eClass1.isSuperTypeOf(eClass2);
        if (!superType) {
            if (eClass1.getName().equals(eClass2.getName())) {
                superType = true;
            } else {
                EList<EClass> superTypes = eClass2.getESuperTypes();
                superTypes.removeAll(visited);
                visited.addAll(superTypes);
                for (EClass st : superTypes) {
                    superType = isSuperType(eClass1, st, visited);
                    if (superType) {
                        return superType;
                    }
                }
            }
        }
        return superType;
    }
	
	protected IScope scope_JvmParametrizedTypeReference_type(EObject context, EReference reference){
		SprayStyleRef style = EcoreUtil2.getContainerOfType(context, SprayStyleRef.class);
		if(style != null){
			return scope_ShapeStyleRefScope(style, context, reference);
		}
		Shape shape = EcoreUtil2.getContainerOfType(context, Shape.class);
		if(shape != null){
			return scope_ShapeShapeScope(shape, context, reference);
		}
		Connection connection = EcoreUtil2.getContainerOfType(context, Connection.class);
		if(connection != null){
			return scope_ShapeConnectionScope(connection, context, reference);
		}
		Action action = EcoreUtil2.getContainerOfType(context, Action.class);
		if(action != null){
			return scope_ActionScope(action, context, reference); 
		}
		return IScope.NULLSCOPE;
	}
	
	/**
     * Restrict the scope of styles to the Styles implementing ISprayStyle
     * 
     * @param style
     * @param context
     * @param reference
     * @return
     */
	protected IScope scope_ShapeStyleRefScope(SprayStyleRef style, EObject context, EReference reference) {
		IScope typesScope = delegateGetScope(style, TypesPackage.Literals.JVM_PARAMETERIZED_TYPE_REFERENCE__TYPE);
		Predicate<IEObjectDescription> stylesFilter = new StyleScopeRestrictor();
		return new FilteringScope(typesScope, stylesFilter);
	}
	
	/**
     * Restrict the scope of shapes to the Shapes implementing ISprayShape
     * 
     * @param shape
     * @param context
     * @param reference
     * @return
     */
	protected IScope scope_ShapeShapeScope(Shape shape, EObject context, EReference reference) {
		IScope typesScope = delegateGetScope(shape, TypesPackage.Literals.JVM_PARAMETERIZED_TYPE_REFERENCE__TYPE);
		Predicate<IEObjectDescription> shapesFilter = new ShapeScopeRestrictor();
		return new FilteringScope(typesScope, shapesFilter);
	}
	
	/**
     * Restrict the scope of connections to the Shapes implementing ISprayConnection
     * 
     * @param connection
     * @param context
     * @param reference
     * @return
     */
	protected IScope scope_ShapeConnectionScope(Connection connection, EObject context, EReference reference) {
		IScope typesScope = delegateGetScope(connection, TypesPackage.Literals.JVM_PARAMETERIZED_TYPE_REFERENCE__TYPE);
		Predicate<IEObjectDescription> connectionFilter = new ConnectionScopeRestrictor();
		return new FilteringScope(typesScope, connectionFilter);
	}
	
	protected IScope scope_ActionScope(Action action, EObject context, EReference reference) {
		IScope typesScope = delegateGetScope(action, TypesPackage.Literals.JVM_PARAMETERIZED_TYPE_REFERENCE__TYPE);
		Predicate<IEObjectDescription> actionFilter = new ActionScopeRestrictor();
		return new FilteringScope(typesScope, actionFilter);
	}
	
	protected IScope scope_ShapeDslKey_jvmKey(EObject context){
		JvmType jvmType = null;
		final String className = "TextIds";
		final Shape shape = EcoreUtil2.getContainerOfType(context, Shape.class);
		final Connection connection = EcoreUtil2.getContainerOfType(context, Connection.class);
		if(shape != null && shape.getShape() != null && shape.getShape().getJvmShape() != null){
			jvmType = shape.getShape().getJvmShape().getType();
		}else if(connection != null && connection.getConnection() != null && connection.getConnection().getJvmConnection() != null){
			jvmType = connection.getConnection().getJvmConnection().getType();
		}
		
		if(jvmType != null && jvmType instanceof JvmGenericType){
			return getEnumerationLiteralsScopeForShape((JvmGenericType) jvmType, className);
		}else{
			return IScope.NULLSCOPE;
		}
	}

	private IScope getEnumerationLiteralsScopeForShape(JvmGenericType type, String className) {
		JvmEnumerationType enumType = null;
		for(JvmMember member : type.getMembers()){
			if(member.getSimpleName().equals(className)){
				enumType = (JvmEnumerationType) member;
			}
		}
		
		List<IEObjectDescription> descrList = new ArrayList<IEObjectDescription>();
		if(enumType != null){
			for(JvmEnumerationLiteral literal : enumType.getLiterals()){
				IEObjectDescription description = EObjectDescription.create(literal.getSimpleName(), literal, null);
				descrList.add(description);
			}
		}
		return MapBasedScope.createScope(IScope.NULLSCOPE, descrList);
	}
	
	protected IScope scope_ShapeCompartment_nestedShape(EObject context){
		Node node = EcoreUtil2.getContainerOfType(context, Node.class);
		if(node != null){
			Predicate<EObject> filterPredicate = new Predicate<EObject>(){
				public boolean apply(EObject input){
					return input instanceof EReference;
				}
			};
			return MapBasedScope.createScope(IScope.NULLSCOPE, Scopes.scopedElementsFor(Iterables.filter(node.getType().getEAllReferences(), filterPredicate)));
		}else{
			return IScope.NULLSCOPE;
		}
	}
}
