package org.eclipselabs.spray.xtext.scoping;

import org.eclipse.graphiti.features.IFeature;
import org.eclipse.xtext.common.types.JvmGenericType;
import org.eclipse.xtext.common.types.JvmTypeReference;
import org.eclipse.xtext.resource.IEObjectDescription;

import com.google.common.base.Predicate;

public class ActionScopeRestrictor implements Predicate<IEObjectDescription>{
	
	private final String ACTION_INTERFACE = IFeature.class.getName();
	
	public boolean apply(IEObjectDescription input) {
		if(input.getEObjectOrProxy() instanceof JvmGenericType){
			return isAction((JvmGenericType) input.getEObjectOrProxy());
		}else{
			return false;
		}
	}
	
	private boolean isAction(JvmGenericType type){
		for(JvmTypeReference itfRef : type.getExtendedInterfaces()){
			if(isAction(itfRef)){
				return true;
			}
		}
		
		for(JvmTypeReference superTypeRef : type.getSuperTypes()){
			if(isAction(superTypeRef)){
				return true;
			}
		}
		
		return false;
	}
	
	private boolean isAction(JvmTypeReference typeRef){
		if(ACTION_INTERFACE.equals(typeRef.getIdentifier())){
			return true;
		}
		
		JvmGenericType type = (JvmGenericType) typeRef.getType();
		for (JvmTypeReference itfRef : type.getExtendedInterfaces()) {
            if (ACTION_INTERFACE.equals(itfRef.getIdentifier())) {
                return true;
            }
        }
		
        for (JvmTypeReference superTypeRef : type.getSuperTypes()) {
            if (isAction(superTypeRef)) {
                return true;
            }
        }
        return false;
	}

}
