/** ****************************************************************************
 * Copyright (c)  The Spray Project.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Spray Dev Team - initial API and implementation
 **************************************************************************** */
package org.eclipselabs.spray.styles.generator.jointjs

import org.eclipselabs.spray.styles.Style
import org.eclipselabs.spray.styles.YesNoBool
import org.eclipselabs.spray.styles.LineStyle
import org.eclipselabs.spray.styles.StyleLayout
import org.eclipselabs.spray.styles.Transparent
import org.eclipselabs.spray.styles.ColorConstantRef
import org.eclipselabs.spray.styles.RGBColor
import org.eclipselabs.spray.styles.GradientRef
import org.eclipselabs.spray.styles.GradientAllignmentimport org.eclipselabs.spray.styles.ColorOrGradient

class StyleGenerator {

    
	def filepath() {
		"style.js"
	}
	
	def compile(Style s) {
		'''
			«s.body»
		'''
	}
	
	def headDia() {
		'''
		function getDiagramHighlighting(stylename) {
		
			var highlighting;
		
			switch(stylename) {
		
		'''
	}
	
	def compileDia(Style s) {
		'''
		«IF s.layout.highlighting != null»
				case "«s.name»":
					var highlighting = '«selected(s)»«multiselected(s)»«allowed(s)»«unallowed(s)»';
				break;
		«ENDIF»
		'''
	}
	
	def selected(Style s) {
		
		if(s.layout.highlighting.selected != null){
			'''.free-transform { border: 2px dashed «s.layout.highlighting.selected.createColorValue»;	}'''
		}
	}
	
	def multiselected(Style s) {
		if(s.layout.highlighting.multiselected != null){
			'''.selection-box { border: 2px solid «s.layout.highlighting.multiselected.createColorValue»; }'''
		}
	}
	
	def allowed(Style s){
		if(s.layout.highlighting.allowed != null){
			'''.linking-allowed { outline: 2px solid «s.layout.highlighting.allowed.createColorValue»; }'''
		}
	}
	
	def unallowed(Style s){
		if(s.layout.highlighting.unallowed != null){
			'''.linking-unallowed { outline: 2px solid «s.layout.highlighting.unallowed.createColorValue»; }'''
		}		
	}
	
	def footerDia() {
		'''
				default:
					highlighting = '';
				break;
					
			}
			return highlighting;
		}		
		'''
	}
	
	
	def head() {
		'''
			function getStyle(stylename) {
				
				var style;
				
				switch(stylename) {
		'''
	}
	
	def body(Style s) {
		'''
					/* 
					This is a generated JavaScript file for the spray JointJS online editor.
					The «s.name» function will be called when the shapes are created, setting style attributes.
					«s.description»
					*/
					case "«s.name»":
						style = {
							'.': { filter: Stencil.filter },
		
							«s.createFontAttributes»
		
							«s.createRectangleAttributes»
							
							«s.createRoundedRectangleAttributes»
		
							«s.createCirleAttributes»
		
							«s.createEllipseAttributes»
							
							«s.createLineAttributes»
							
							«s.createConnectionAttributes»
							
							«s.createPolygonAttributes»
							
							«s.createPolylineAttributes»
							
							«createBoundingBoxStyle»
						};
					break;
		'''
	}
	
	def footer() {
		'''
					default:
						style = {};
					break;
					
				}
				return style;
			}
		'''
	}
	
	def createFontAttributes(Style s) {
		'''
			/*
			Generated text style attributes.
			*/
			«IF s.name != null»
				text: {
					«fontAttributes(s.layout)»
				},
			«ENDIF»
		'''
	}
	
	def fontAttributes(StyleLayout s){
		'''
		'font-family': '«IF s.fontName != null»«s.fontName»«ELSE»sans-serif«ENDIF»',
		'font-size': «IF s.fontSize > 0»«s.fontSize»«ELSE»'11px'«ENDIF»,
		fill: '«IF s.fontColor != null»«s.fontColor.createColorValue»«ELSE»#000000«ENDIF»',
		'font-weight': '«IF s.fontBold == YesNoBool.YES»700«ELSE»400«ENDIF»',
		«IF s.fontItalic == YesNoBool.YES»
			'font-style': 'italic',
		«ENDIF»
		'''
	}
	
	def createRectangleAttributes(Style s) {
		'''
			/*
			Generated rectangle style attributes.
			*/
			rect: {
				«s.layout.commonAttributes»
			},
		'''
	}

	def createRoundedRectangleAttributes(Style s) {
		'''
			/*
			Generated rectangle style attributes.
			*/
			rect: {
				«s.layout.commonAttributes»
			},
		'''
	}
	
	
	def createCirleAttributes(Style s) {
		'''
			/*
			Generated circle style attributes.
			*/
			circle: {
				«s.layout.commonAttributes»
			},
		'''
	}

	def createPolygonAttributes(Style s) {
		'''
			/*
			Generated circle style attributes.
			*/
			polygon: {
				«s.layout.commonAttributes»
			},
		'''
	}

	def createPolylineAttributes(Style s) {
		'''
			/*
			Generated circle style attributes.
			*/
			polyline: {
				«s.layout.commonAttributes»
			},
		'''
	}	
	
	def createEllipseAttributes(Style s) {
		'''
			/*
			Generated ellipse style attributes.
			*/
			ellipse: {
				«s.layout.commonAttributes»
			},
		'''
	}
	
	def createLineAttributes(Style s) {
		'''
			/*
			Generated ellipse style attributes.
			*/
			line: {
				«s.layout.commonAttributes»
			},
		'''
	}	
	
	def createConnectionAttributes(Style s) {
		'''
			/*
			Generated Connection style attributes.
			*/
			'.connection': {
				«s.layout.commonAttributes»
			},
		'''
	}
	
	def createBoundingBoxStyle() {
		'''
		/*
		Generated bounding box styles.
		*/
		'.bounding-box':{
			'stroke-width': 0,
			fill: 'transparent'
		}
		'''
	}
	
	def commonAttributes(StyleLayout s) {
		'''
			«IF s.checkBackgroundGradientNecessary»
				«s.background.createGradientAttributes(s.gradient_orientation.gradientOrientation)»
			«ELSE»
				«s.createBackgroundAttributes»
			«ENDIF»

			«s.createLineAttributes»
		'''
	}
	
	def checkBackgroundGradientNecessary(StyleLayout l) {
		if (!(l.background instanceof GradientRef)) {
            return false
        } else {
            return true
        }
	}
	
	def dispatch createGradientAttributes(GradientRef gr, boolean horizontal) {
		'''
			fill: {
				type: 'linearGradient',
				stops: [
					«FOR area : gr.gradientRefFromDsl.layout.area»
					{ offset: '«area.offset»', color: '«area.color.createColorValue»' },
					«ENDFOR»
				«IF horizontal»
					]
				«ELSE»
					],
					attrs: {
						x1: '0%',
						y1: '0%',
						x2: '0%',
						y2: '100%'	
					}
				«ENDIF»
			},
		'''
	}
	
	def createBackgroundAttributes(StyleLayout l) {
		'''
			«IF l == null || l.background == null»
			«ELSE»
				fill: '«l.background.createColorValue»',
				'fill-opacity': «l.background.createOpacityValue»,
			«ENDIF»
		'''
	}
	

	
	def createLineAttributes(StyleLayout l) {
		'''
			«IF l == null || l.lineColor == null»
				stroke: '#000000',
				'stroke-width': 0,
				'stroke-dasharray': "0",
			«ELSEIF l.lineColor instanceof Transparent»
				'stroke-opacity': 0,
			«ELSE»
				stroke: '«l.lineColor.createColorValue»',
				«IF l.lineWidth > 0»
					'stroke-width': «l.lineWidth»,
				«ENDIF»
				«IF l.lineStyle != LineStyle::NULL»
					«IF l.lineStyle.equals(LineStyle.DASH)»
						'stroke-dasharray': "10,10",
					«ELSEIF l.lineStyle.equals(LineStyle.DOT)»
						'stroke-dasharray': "5,5",
					«ELSEIF l.lineStyle.equals(LineStyle.DASHDOT)»
						'stroke-dasharray': "10,5,5,5",
					«ELSEIF l.lineStyle.equals(LineStyle.DASHDOTDOT)»
						'stroke-dasharray': "10,5,5,5,5,5",
					«ENDIF»
				«ELSE»
					'stroke-dasharray': "0",
				«ENDIF»
			«ENDIF»
		'''
	}
	
	def gradientOrientation(GradientAllignment ga) {
		ga.equals(GradientAllignment.HORIZONTAL)
	}
	
	def dispatch createColorValue(ColorConstantRef c) {
        val color = c.value
        switch(color){
        	case WHITE: '''#ffffff'''
        	case LIGHT_LIGHT_GRAY: '''#e9e9e9''' //half way between light gray and white
        	case LIGHT_GRAY: '''#d3d3d3'''
        	case GRAY: '''#808080'''
        	case DARK_GRAY: '''#a9a9a9'''
        	case BLACK: '''#000000'''
        	case RED: '''#ff0000'''
        	case LIGHT_ORANGE: '''#ffa07a''' //light salmon
        	case ORANGE: '''#ffa500'''
        	case DARK_ORANGE: '''#ff8c00'''
        	case YELLOW: '''#ffff00'''
        	case GREEN: '''#008000'''
        	case LIGHT_GREEN: '''#90EE90'''
        	case DARK_GREEN: '''#006400'''
        	case CYAN: '''#00ffff'''
        	case LIGHT_BLUE: '''#add8e6'''
        	case BLUE: '''#0000ff'''
        	case DARK_BLUE: '''#00008b'''
        }
    }

    def dispatch createColorValue(RGBColor c) {
        '''#«String.format("%02X", c.red)»«String.format("%02X", c.green)»«String.format("%02X", c.blue)»'''
    }
    
    def dispatch createColorValue(Transparent c) {
    	'''transparent'''
    }
	
	def dispatch createGradientAttributes(Transparent cg, boolean horizontal) { }
	
	def dispatch createOpacityValue(Transparent c){
		'''0.0'''
	}
	
	def dispatch createOpacityValue(ColorOrGradient c){
		'''1.0'''
	}

}