/** ****************************************************************************
 * Copyright (c)  The Spray Project.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Spray Dev Team - initial API and implementation
 **************************************************************************** */
package org.eclipselabs.spray.generator.graphiti.templates.features

import javax.inject.Inject
import org.eclipse.emf.ecore.EAttribute
import org.eclipse.emf.ecore.EDataType
import org.eclipse.emf.ecore.EReference
import org.eclipselabs.spray.generator.graphiti.util.NamingExtensions
import org.eclipselabs.spray.generator.graphiti.util.mm.MetaClassExtensions
import org.eclipselabs.spray.MetaClass
import org.eclipselabs.spray.xtext.generator.FileGenerator

import static org.eclipselabs.spray.common.generator.GeneratorUtil.*
import org.eclipselabs.spray.Node
import org.eclipselabs.spray.Edge

class CreateShapeFeature extends FileGenerator<MetaClass> {
    @Inject extension NamingExtensions
    @Inject extension MetaClassExtensions
    
    override CharSequence generateBaseFile(MetaClass modelElement) {
        mainFile( modelElement, javaGenFile.baseClassName)
    }

    override CharSequence generateExtensionFile(MetaClass modelElement) {
        mainExtensionPointFile( modelElement, javaGenFile.className)
    }
    
    def mainExtensionPointFile(MetaClass metaClass, String className) '''    
        «extensionHeader(this)»
        package «feature_package()»;
        
        import org.eclipse.graphiti.features.IFeatureProvider;
        
        public class «className» extends «className»Base {
            public «className»(final IFeatureProvider fp) {
                super(fp);
            }
        }
    '''

    def mainFile (MetaClass metaClass, String className) '''
        «val diagram = getDiagram(metaClass)»
        «header(this)»
        package «feature_package()»;

        import org.eclipse.graphiti.features.IFeatureProvider;
        import org.eclipse.graphiti.features.context.ICreateContext;
        import org.eclipse.graphiti.mm.pictograms.Diagram;
        import org.eclipselabs.spray.runtime.graphiti.containers.SampleUtil;
        import org.eclipselabs.spray.runtime.graphiti.features.AbstractCreateFeature;
        import org.eclipselabs.spray.runtime.graphiti.layout.SprayLayoutService;
        import org.eclipselabs.spray.runtime.graphiti.GraphitiProperties;
        import org.eclipse.graphiti.features.context.IAreaContext;
        import org.eclipse.graphiti.mm.pictograms.PictogramElement;
        import org.eclipse.graphiti.features.context.impl.AddContext;
        import org.eclipse.graphiti.mm.pictograms.ContainerShape;
        // MARKER_IMPORT
        
        public abstract class «className» extends AbstractCreateFeature {
            protected static String TITLE = "Create «metaClass.uiLabel»";
            protected static String USER_QUESTION = "Enter new «metaClass.uiLabel» name";
            protected «diagram.modelServiceClassName.shortName» modelService;
            protected «metaClass.itfName» newClass = null;
            «generate_additionalFields(metaClass)»
        
        
            public «className» (final IFeatureProvider fp) {
                // set name and description of the creation feature
                super(fp, "«metaClass.name»", "«metaClass.createFeatureDescription»");
                modelService = «diagram.modelServiceClassName.shortName».getModelService(fp.getDiagramTypeProvider());
            }
        
            «generate_canCreate(metaClass)»
            «generate_create(metaClass)»
            «generate_createModelElement(metaClass)»
            «generate_getCreateImageId(metaClass)»
            «generate_addGraphicalRepresentation(metaClass)»
            «generate_additionalMethods(metaClass)»
        }
    '''
    
    def getDiagram(MetaClass metaClass) {
    	if (metaClass instanceof Node) {
    		return metaClass.diagram
    	}
    	else if (metaClass instanceof Edge) {
    		return metaClass.diagram
    	}
    }
    
    
    /**
     * Determine the name that appears in the dialog when asking for the name
     */
    def private getUiLabel (MetaClass mc) {
        mc.name
    }

    def generate_canCreate (MetaClass metaClass) '''
        «overrideHeader()»
        public boolean canCreate(final ICreateContext context) {
            final Object target = getBusinessObjectForPictogramElement(context.getTargetContainer());
            «IF metaClass.container != null»
                if (context.getTargetContainer() instanceof Diagram) {
                    «IF metaClass.container.containmentReference != null»
                        return true;
                    «ELSE»    
                        return false;
                    «ENDIF»
                }
            «ENDIF»
            «IF metaClass instanceof Node»
	            «var result = metaClass.referencesTo»
	            «FOR cls : result »
	                // cls «cls.shape.node.name» refers to this metaClass
	                «IF cls.nestedShape.containment»
	                if( target instanceof «cls.shape.node.javaInterfaceName» ){
	                    if (SprayLayoutService.isCompartment(context.getTargetContainer())) {
	                        String id = GraphitiProperties.get(context.getTargetContainer(), TEXT_ID);
	                        if ( (id != null) && (id.equals("«cls.compartment.simpleName»")) ) {
	                            return true;
	                        }
	                    }
	                }
	                «ENDIF»
	            «ENDFOR»
	        «ENDIF»
            
            return false;
        }
    '''

    
    def generate_create (MetaClass metaClass) '''
        «overrideHeader()»
        public Object[] create(final ICreateContext context) {
            newClass = create«metaClass.name»(context);

            if (newClass == null ) {
                return EMPTY;
            }

            // do the add
            addGraphicalRepresentation(context, newClass);
            
            // activate direct editing after object creation
            getFeatureProvider().getDirectEditingInfo().setActive(true);
            
            // return newly created business object(s)
            return new Object[] { newClass };
        }
    '''
    
    def generate_createModelElement (MetaClass metaClass) '''
        «val diagram = metaClass.diagram»
        «val modelClassName = diagram.modelType.itfName»
		
		/**
		* Creates a new {@link «metaClass.itfName»} instance and adds it to the containing type.
		*/
		protected «metaClass.itfName» create«metaClass.name»(final ICreateContext context) {
		// create «metaClass.name» instance
        final «metaClass.itfName» newClass = «metaClass.EFactoryInterfaceName.shortName».eINSTANCE.create«metaClass.name»();
            «if(metaClass.onCreate != null) { handleAskFor(metaClass, metaClass.onCreate.askFor) }»
		ContainerShape targetContainer = context.getTargetContainer();
		boolean isContainment = false;
		final Object target = getBusinessObjectForPictogramElement(context.getTargetContainer());
		
		«IF metaClass instanceof Node»
			«var result = metaClass.referencesTo»
            «FOR cls : result »
            «var domainName = cls.shape.node.javaInterfaceName»
               «IF cls.nestedShape.containment»
                if( target instanceof «domainName» ){
                    «domainName» domainObject = («domainName») target;
                        // containment
                        «IF cls.nestedShape.many»
                            domainObject.get«cls.nestedShape.name.toFirstUpper»().add(newClass);
                            setDoneChanges(true);
                            return newClass;
                        «ELSE»
                            domainObject.set«cls.nestedShape.name.toFirstUpper»(newClass);
                            // clear the shapes for the previous «cls.nestedShape.name.toFirstUpper»
                            targetContainer.getChildren().clear();
                            setDoneChanges(true);
                            return newClass;
                        «ENDIF»
                    }
                «ELSE»
                // NOT containment 
                «ENDIF»
            «ENDFOR»
		«ENDIF»
            «IF metaClass.container != null && metaClass.container.containmentReference != null»
		if (!isContainment) {
			// add the element to containment reference
                «modelClassName» model = modelService.getModel();
                «IF metaClass.container.containmentReference.many»
			model.get«metaClass.container.containmentReference.name.toFirstUpper»().add(newClass);
                «ELSE»
                // clear the shapes for the previous model
                targetContainer.getChildren().clear();
                model.set«metaClass.container.containmentReference.name.toFirstUpper»(newClass);
                «ENDIF»   
		}
            «ENDIF»
		setDoneChanges(true);
		return newClass;
		}
    '''
    
    def handleAskFor(MetaClass metaClass, EAttribute attribute) '''
        «IF attribute != null»
           // ask user for «metaClass.name» «attribute.name»
           «IF (attribute.EType as EDataType).instanceClassName.matches('java.lang.String')»
              String new«attribute.name.toFirstUpper» = SampleUtil.askString(TITLE, USER_QUESTION, "", null);
              if (new«attribute.name.toFirstUpper» == null || new«attribute.name.toFirstUpper».trim().length() == 0) {
                 return null;
              } else {
                 newClass.set«attribute.name.toFirstUpper»(new«attribute.name.toFirstUpper»);
              }
           «ELSE»
              «val type = (attribute.EType as EDataType).instanceClassName» 
              «val typeName = if("double".matches(type)) "Double" else if("int".matches(type)) "Integer" else "Object"»
              final «"org.eclipse.jface.dialogs.IInputValidator".shortName» validator = new IInputValidator() {
                 public String isValid(final String _newText) {
                    String message = null;
                    try {
                       «typeName».valueOf(_newText);
                    } catch(Exception nfe) {
                       message = _newText + " cannot be cast to «typeName»";
                    }
                    return message;
                 }
              };
              final String new«attribute.name.toFirstUpper»String = SampleUtil.askString(TITLE, USER_QUESTION, "", validator);
              final «typeName» new«attribute.name.toFirstUpper» = «typeName».valueOf(new«attribute.name.toFirstUpper»String);    
              newClass.set«attribute.name.toFirstUpper»(new«attribute.name.toFirstUpper»);
           «ENDIF»
        «ENDIF»
    '''
 
 
    /**
     * When a MetaClass is aliased we need to put a property with the alias name into the AddContext in order to
     * return the right AddFeature in the FeatureProvider
     */
    def generate_addGraphicalRepresentation (MetaClass metaClass) '''
        «overrideHeader()»
        protected PictogramElement addGraphicalRepresentation(final IAreaContext context, final Object newObject) {
            final AddContext ctx = new AddContext(context, newObject);
            ctx.putProperty(PROPERTY_ALIAS, "«metaClass.name»");
            return getFeatureProvider().addIfPossible(ctx);
        }
    '''
    
    def generate_getCreateImageId (MetaClass metaClass) '''
        «val diagram = metaClass.diagram»
        «IF (metaClass.icon != null)»
            «overrideHeader()»
            public String getCreateImageId() {
                return «diagram.imageProviderClassName.shortName».«diagram.getImageIdentifier(metaClass.icon)»;
            }
        «ENDIF»
    '''
    
}