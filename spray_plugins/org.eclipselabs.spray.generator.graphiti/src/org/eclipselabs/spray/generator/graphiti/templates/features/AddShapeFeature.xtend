/** ****************************************************************************
 * Copyright (c)  The Spray Project.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Spray Dev Team - initial API and implementation
 **************************************************************************** */
package org.eclipselabs.spray.generator.graphiti.templates.features

import javax.inject.Inject
import org.eclipselabs.spray.generator.graphiti.util.NamingExtensions
import org.eclipselabs.spray.Shape
import org.eclipselabs.spray.styles.Style
import org.eclipselabs.spray.xtext.generator.FileGenerator

import static org.eclipselabs.spray.common.generator.GeneratorUtil.*
import org.eclipse.emf.ecore.EObject
import org.eclipselabs.spray.SprayStyleRef
import org.eclipselabs.spray.shapes.ShapeStyleRef
import org.eclipselabs.spray.Node

class AddShapeFeature extends FileGenerator<Shape> {
    
    @Inject extension NamingExtensions
    
    Node node = null
    EObject style = null
    
    def setAttributes(Node cls, Style style){
        node = cls
        this.style = style
    }

    def setAttributes(Node cls, SprayStyleRef style){
        node = cls
        this.style = style
    }

    def setAttributes(Node cls, ShapeStyleRef style){
        node = cls
        this.style = style
    }
    
    override CharSequence generateBaseFile(Shape modelElement) {
        mainFile( modelElement as Shape, javaGenFile.baseClassName)
    }

    override CharSequence generateExtensionFile(Shape modelElement) {
        mainExtensionPointFile( modelElement as Shape, javaGenFile.className)
    }
    
    def mainExtensionPointFile(Shape container, String className) '''    
        «extensionHeader(this)»
        package «feature_package()»;
        
        import org.eclipse.graphiti.features.IFeatureProvider;
        
        public class «className» extends «className»Base {
            public «className»(final IFeatureProvider fp) {
                super(fp);
            }
        }
    '''

    def mainFile(Shape container, String className) '''
        «header(this)»
        package «feature_package()»;
        
        import org.eclipse.emf.ecore.EObject;
        import org.eclipse.graphiti.features.IFeatureProvider;
        import org.eclipse.graphiti.features.context.IAddContext;
        import org.eclipse.graphiti.mm.pictograms.ContainerShape;
        import org.eclipse.graphiti.mm.pictograms.Shape;
        import org.eclipse.graphiti.mm.pictograms.Diagram;
        import org.eclipse.graphiti.mm.pictograms.PictogramElement;
        import org.eclipse.graphiti.services.Graphiti;
        import org.eclipse.graphiti.services.IGaService;
        import org.eclipselabs.spray.runtime.graphiti.features.AbstractAddFeature;
        import org.eclipselabs.spray.runtime.graphiti.shape.ISprayShape;
        import org.eclipselabs.spray.runtime.graphiti.layout.SprayLayoutService;
        import org.eclipselabs.spray.runtime.graphiti.GraphitiProperties;
        import «container.shape.qualifiedName»Shape;
        import org.eclipselabs.spray.runtime.graphiti.styles.ISprayStyle;
        // MARKER_IMPORT

        @SuppressWarnings("unused")
        public abstract class «className» extends AbstractAddFeature {
            protected final static String typeOrAliasName = "«node.name»";
            protected Diagram targetDiagram = null;
        
            public «className»(final IFeatureProvider fp) {
                super(fp);

            }
        
            «overrideHeader()»
            public boolean canAdd(final IAddContext context) {
                final EObject newObject = (EObject) context.getNewObject();
                if (newObject instanceof «node.itfName») {
                    // check if user wants to add to a diagram
                    if (context.getTargetContainer() instanceof Diagram) {
                        «IF (node.container != null && node.container.containmentReference != null)»
                        return true;
                        «ELSE»
                        return false;
                        «ENDIF»
                    } else if (context.getTargetContainer() instanceof ContainerShape) {
                    	final Object target = getBusinessObjectForPictogramElement(context.getTargetContainer());
                    	«var result = node.referencesTo»
                        «FOR sc : result »
                        // cls «sc.shape.node.itfName» refers to this metaClass»
                        if( target instanceof «sc.shape.node.javaInterfaceName» ){
                            if (SprayLayoutService.isCompartment(context.getTargetContainer())) {
                                String id = GraphitiProperties.get(context.getTargetContainer(), TEXT_ID);
                                if ( (id != null) && (id.equals("«sc.compartment.simpleName»")) ) {
                                    return true;    
                                }
                            }
                        }
                        «ENDFOR»	
                    }
                }
                return false;
            }

            «overrideHeader»
            public PictogramElement add(final IAddContext context) {
                final «node.itfName» addedModelElement = («node.itfName») context.getNewObject();
                        // NEW stuff
                Object target = getBusinessObjectForPictogramElement(context.getTargetContainer());
                final ContainerShape targetContainer = context.getTargetContainer();
                «IF node.style != null»
                final ISprayStyle style = new «node.style.qualifiedName»();
                «ELSEIF style != null »
                final ISprayStyle style = new «style.qualifiedName»();
                «ELSE»
                final ISprayStyle style = new org.eclipselabs.spray.runtime.graphiti.styles.DefaultSprayStyle();
                «ENDIF»
                final ISprayShape shape = new «container.shape.simpleName»Shape(getFeatureProvider());
                final ContainerShape conShape = shape.getShape(targetContainer, style);
                final IGaService gaService = Graphiti.getGaService();
                gaService.setLocation(conShape.getGraphicsAlgorithm(), context.getX(), context.getY());
                link(conShape, addedModelElement);
                linkShapes(conShape, addedModelElement);
                peService.setPropertyValue(conShape , PROPERTY_ALIAS, "«node.name»");

                setDoneChanges(true);
                updatePictogramElement(conShape);
                layout(conShape);
                
                return conShape;
            }
        
        
            protected void linkShapes(ContainerShape conShape, «node.itfName» addedModelElement) {
                link(conShape, addedModelElement);
                for (Shape childShape : conShape.getChildren()) {
                   if( childShape instanceof ContainerShape ) {
                      linkShapes((ContainerShape)childShape, addedModelElement);
                   } else {
                      link(childShape, addedModelElement);
                   }
                }
            }
        }
        '''
        
}