/** ****************************************************************************
 * Copyright (c)  The Spray Project.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Spray Dev Team - initial API and implementation
 **************************************************************************** */
package org.eclipselabs.spray.generator.graphiti.util.mm

import org.eclipse.emf.ecore.EObject
import static org.eclipse.xtext.EcoreUtil2.*

import org.eclipselabs.spray.generator.graphiti.templates.features.CreateConnectionFeature
import org.eclipselabs.spray.generator.graphiti.templates.features.CreateShapeFeature
import org.eclipselabs.spray.Diagram
import org.eclipselabs.spray.Action
import org.eclipselabs.spray.generator.graphiti.templates.features.CustomFeature
import org.eclipselabs.spray.Node
import org.eclipselabs.spray.Edge

class DiagramExtensions {
    
    def Diagram getDiagram (EObject ctx) {
        getContainerOfType(ctx, typeof(Diagram))
    }
    
    /**
     * Spray MetaClass can either be represented by a Connection or any other kind of Shape.
     * This method will return those MetaClasses of a Diagram that <i>are <b>not</b></i> represented by a Connection.
     */
    def Iterable<Node> getNodes (Diagram diagram) {
        diagram.nodes
    }
    
    /**
     * Spray MetaClass can either be represented by a Connection or any other kind of Shape.
     * This method will return those MetaClasses of a Diagram that <i>are</i> represented by a Connection.
     */
    def Iterable<Edge> getEdges (Diagram diagram) {
        diagram.edges
    }
    
    def dispatch Iterable<Node> getElementsForTemplate (Diagram diagram, CreateShapeFeature template) {
        getNodes(diagram)
    }
    
    def dispatch Iterable<Edge> getElementsForTemplate (Diagram diagram, CreateConnectionFeature template) {
        // FIXME diagram.metaClasses.filter(mc|mc.representedBy instanceof ConnectionInSpray && mc.behaviors.exists(b|b instanceof CreateBehavior))
        getEdges(diagram)
    }
    
    /**
     * Returns all generateable CustomBehavior instances for the diagram. Not generateable are those that are realized by a concrete JVM type
     */
    def Iterable<Action> getBehaviorsForTemplate (Diagram diagram, CustomFeature template) {
        // FIXME diagram.metaClasses.map(mc|mc.allBehaviors).flatten.toSet.filter(typeof(CustomBehavior)).filter(b|b.realizedBy==null)
        
        diagram.globalActionGroups.map(g|g.actions).flatten +
	    diagram.nodes.filter(n|n.actionGroup != null).map(n|n.actionGroup.actions).flatten +
       	diagram.edges.filter(e|e.actionGroup != null).map(e|e.actionGroup.actions).flatten
    }
    
    
}