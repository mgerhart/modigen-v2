/** ****************************************************************************
 * Copyright (c)  The Spray Project.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Spray Dev Team - initial API and implementation
 **************************************************************************** */
package org.eclipselabs.spray.generator.graphiti.templates.features

import org.eclipselabs.spray.Action
import org.eclipselabs.spray.xtext.generator.FileGenerator

import static org.eclipselabs.spray.common.generator.GeneratorUtil.*


class CustomFeature extends FileGenerator<Action>  {
    
    override CharSequence generateBaseFile(Action action) {
        mainFile( action as Action, javaGenFile.baseClassName)
    }

    override CharSequence generateExtensionFile(Action action) {
        mainExtensionPointFile(action, javaGenFile.className)
    }
    
    def mainExtensionPointFile(Action action, String className) '''    
        «extensionHeader(this)»
        package «feature_package()»;
        
        import org.eclipse.emf.ecore.EObject;
        import org.eclipse.graphiti.features.IFeatureProvider;
        import org.eclipse.graphiti.features.context.ICustomContext;
        
        public class «className» extends «className»Base {
            public «className»(final IFeatureProvider fp) {
                super(fp);
            }
        
            «overrideHeader»
            public void execute(final ICustomContext context, final EObject object) {
                // TODO add code here.
            }
        
        }
    '''

    def mainFile(Action action, String className) '''
        «header(this)»
        package «feature_package()»;
        
        import org.eclipse.emf.ecore.EObject;
        import org.eclipse.graphiti.features.IFeatureProvider;
        import org.eclipse.graphiti.features.context.ICustomContext;
        import org.eclipse.graphiti.mm.pictograms.PictogramElement;
        import org.eclipselabs.spray.runtime.graphiti.features.AbstractCustomFeature;
        // MARKER_IMPORT
        
        public abstract class «className» extends AbstractCustomFeature {
            «generate_additionalFields(action)»
        
            public «className»(final IFeatureProvider fp) {
                super(fp);
            }
        
            «overrideHeader»
            public String getName() {
                return "«action.label»"; //$NON-NLS-1$
            }
        
            «overrideHeader»
            public String getDescription() {
                return "«action.label» description"; //$NON-NLS-1$
            }
        
            «generate_canExecute()»
            «generate_execute()»
            «generate_additionalMethods(action)»
        }
    '''
    
    def generate_canExecute () '''
        «overrideHeader»
        public boolean canExecute(final ICustomContext context) {
            // allow rename if exactly one pictogram element
            // representing an EClass is selected
            boolean ret = true;
            final PictogramElement[] pes = context.getPictogramElements();
            if (pes != null && pes.length == 1) {
                final EObject bo = (EObject) getBusinessObjectForPictogramElement(pes[0]);
                ret = canExecute (context, bo);
            }
            return ret;
        } 

        protected boolean canExecute(final ICustomContext context, final EObject bo) {
            return true;
        }
    '''
    
    def generate_execute () '''
        «overrideHeader»
        public void execute(final ICustomContext context) {
            final PictogramElement[] pes = context.getPictogramElements();
            if (pes != null && pes.length == 1) {
                final EObject bo = (EObject) getBusinessObjectForPictogramElement(pes[0]);
                execute(context, bo);
            }
        }
        
        public abstract void execute(ICustomContext context, EObject object);
    '''
}