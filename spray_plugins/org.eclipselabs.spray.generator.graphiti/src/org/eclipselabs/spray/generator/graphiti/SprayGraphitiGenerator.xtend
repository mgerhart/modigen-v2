/** ****************************************************************************
 * Copyright (c)  The Spray Project.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Spray Dev Team - initial API and implementation
 **************************************************************************** */
package org.eclipselabs.spray.generator.graphiti

import javax.inject.Inject
import com.google.inject.Provider
import org.apache.commons.logging.Log
import org.apache.commons.logging.LogFactory
import org.eclipse.emf.ecore.EAttribute
import org.eclipse.emf.ecore.EClass
import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.xtext.generator.IFileSystemAccess
import org.eclipse.xtext.generator.IGenerator
import org.eclipselabs.spray.common.generator.ProjectProperties
import org.eclipselabs.spray.generator.graphiti.templates.ExecutableExtensionFactory
import org.eclipselabs.spray.generator.graphiti.templates.Filter
import org.eclipselabs.spray.generator.graphiti.templates.GuiceModule
import org.eclipselabs.spray.generator.graphiti.templates.Plugin
import org.eclipselabs.spray.generator.graphiti.templates.PluginActivator
import org.eclipselabs.spray.generator.graphiti.templates.PropertySection
import org.eclipselabs.spray.generator.graphiti.templates.diagram.DiagramTypeProvider
import org.eclipselabs.spray.generator.graphiti.templates.diagram.FeatureProvider
import org.eclipselabs.spray.generator.graphiti.templates.diagram.ImageProvider
import org.eclipselabs.spray.generator.graphiti.templates.diagram.ModelService
import org.eclipselabs.spray.generator.graphiti.templates.diagram.ToolBehaviorProvider
import org.eclipselabs.spray.generator.graphiti.templates.features.AddConnectionFeature
import org.eclipselabs.spray.generator.graphiti.templates.features.AddConnectionFromDslFeature
import org.eclipselabs.spray.generator.graphiti.templates.features.CopyFeature
import org.eclipselabs.spray.generator.graphiti.templates.features.CreateConnectionFeature
import org.eclipselabs.spray.generator.graphiti.templates.features.CreateShapeFeature
import org.eclipselabs.spray.generator.graphiti.templates.features.CustomFeature
import org.eclipselabs.spray.generator.graphiti.templates.features.DirectEditFeature
import org.eclipselabs.spray.generator.graphiti.templates.features.LayoutFromDslFeature
import org.eclipselabs.spray.generator.graphiti.templates.features.MoveFeature
import org.eclipselabs.spray.generator.graphiti.templates.features.PasteFeature
import org.eclipselabs.spray.generator.graphiti.templates.features.ResizeFeature
import org.eclipselabs.spray.generator.graphiti.templates.features.UpdateConnectionFeature
import org.eclipselabs.spray.generator.graphiti.templates.features.UpdateConnectionFromDslFeature
import org.eclipselabs.spray.generator.graphiti.util.NamingExtensions
import org.eclipselabs.spray.generator.graphiti.util.mm.DiagramExtensions
import org.eclipselabs.spray.Connection
import org.eclipselabs.spray.Action
import org.eclipselabs.spray.Diagram
import org.eclipselabs.spray.MetaClass
import org.eclipselabs.spray.Shape
import org.eclipselabs.spray.xtext.generator.filesystem.JavaGenFile
import org.eclipselabs.spray.generator.graphiti.templates.features.AddShapeFeature
import org.eclipselabs.spray.generator.graphiti.templates.features.UpdateShapeFeature

class SprayGraphitiGenerator implements IGenerator {
    @Inject Provider<JavaGenFile> genFileProvider
    @Inject extension NamingExtensions naming
    @Inject extension DiagramExtensions diagramExtensions
    
    @Inject PluginActivator pluginActivator
    @Inject ExecutableExtensionFactory executableExtensionFactory
    @Inject GuiceModule guiceModule
    @Inject Plugin plugin
    @Inject DiagramTypeProvider dtp
    @Inject FeatureProvider fp
    @Inject AddShapeFeature addShapeFromDslFeature
    @Inject AddConnectionFeature addConnectionFeature
    @Inject AddConnectionFromDslFeature addConnectionFromDslFeature
    @Inject CreateConnectionFeature createConnectionFeature
    @Inject CreateShapeFeature createShapeFeature
    @Inject UpdateConnectionFeature updateConnectionFeature
    @Inject UpdateConnectionFromDslFeature updateConnectionFromDslFeature
    @Inject MoveFeature moveFeature
    @Inject LayoutFromDslFeature layoutFromDslFeature
    @Inject UpdateShapeFeature updateShapeFromDslFeature
    @Inject ImageProvider imageProvider
    @Inject ToolBehaviorProvider toolBehaviourProvider
    @Inject PropertySection propertySection
    @Inject Filter filter
    @Inject Filter filter2
    @Inject CustomFeature customFeature
    @Inject ModelService modelService
    @Inject DirectEditFeature directEditFeature
    @Inject CopyFeature copyFeature
    @Inject PasteFeature pasteFeature
    @Inject ResizeFeature resizeFeature
    
    private static Log   LOGGER       = LogFactory::getLog("SprayGraphitiGenerator");
    /**
     * This method is a long sequence of calling all templates for the code generation
     */
    override void doGenerate(Resource resource, IFileSystemAccess fsa) {  
        if (!resource.URI.lastSegment().endsWith(ProjectProperties::SPRAY_FILE_EXTENSION)) {
            LOGGER.info("Spray generator is NOT producing Graphiti code for model " + resource.URI)
            return;
        }
        LOGGER.info("Spray generator is producing Graphiti code for model " + resource.URI)

        ProjectProperties::setModelUri(resource.URI)

        val JavaGenFile java = genFileProvider.get()
        java.access = fsa
        var Diagram diagram = resource.contents.head as Diagram

        generatePluginXml(diagram, fsa)
        generatePluginActivator(diagram, java, pluginActivator)
        generateExectuableExtensionFactory(diagram, java)
        generateModelService(diagram, java)
        generateGuiceModule(diagram, java)
        
        generateDiagramTypeProvider(diagram, java, dtp)
        generateFeatureProvider(diagram, java, fp)
        
        generateAddShapeFeatures(diagram, java, addShapeFromDslFeature)
        generateAddConnectionFeatures(diagram, java, addConnectionFeature)
//        generateAddReferenceAsConnectionFeature(diagram, java, addReferenceAsConnectionFeature)
        
        generateCreateConnectionFeature(diagram, java, createConnectionFeature)
        generateCreateShapeFeature(diagram, java, createShapeFeature)
        
//        generateCreateReferenceAsConnectionFeature(diagram, java, createReferenceAsConnectionFeature)
        generateUpdateAndLayoutFeatures(diagram, java, updateConnectionFeature)
        generateUpdateAndLayoutFromDslFeatures(diagram, java, updateShapeFromDslFeature, layoutFromDslFeature)
//        generateDeleteReferenceFeature(diagram, java, deleteReferenceFeature)
        
        generateImageProvider(diagram, java, imageProvider)
        generateToolBehaviourProvider(diagram, java, toolBehaviourProvider)
        
        generatePropertySection(diagram, java, propertySection)
        generateFilter(diagram, java, filter, filter2)
        
        generateCustomFeature(diagram, java, customFeature)
        generateDirectEditFeature(diagram, java, directEditFeature)
        
        generateCopyFeature(diagram, java, copyFeature)
        generatePasteFeature(diagram, java, pasteFeature)
        generateResizeFeature(diagram, java, resizeFeature)
        generateMoveFeature(diagram, java, moveFeature)
    }

    def generatePluginXml(Diagram diagram, IFileSystemAccess fsa) {
        fsa.generateFile("plugin.xml", plugin.generate(diagram))
        // segments
        fsa.generateFile("plugin_diagramtype.xml", plugin.generate_diagramType(diagram.name))
        fsa.generateFile("plugin_diagramtypeprovider.xml", plugin.generate_diagramTypeProvider(diagram, diagram.name))
        fsa.generateFile("plugin_graphitieditor.xml", plugin.generate_graphitiEditorExtension(diagram))
        fsa.generateFile("plugin_imageprovider.xml", plugin.generate_imageProvider(diagram))
        fsa.generateFile("plugin_newdiagramwizard.xml", plugin.generate_newDiagramWizard(diagram))
        fsa.generateFile("plugin_propertysections.xml", plugin.generate_propertySections(diagram, diagram.name))
        fsa.generateFile("plugin_propertytabs.xml", plugin.generate_propertyTab(diagram.name))
    }
    
    def generatePluginActivator(Diagram diagram, JavaGenFile java, PluginActivator activator) {
        java.hasExtensionPoint = false
        java.setPackageAndClass(diagram.activatorClassName)
        activator.generate(diagram, java)
    }
    
    def generateExectuableExtensionFactory(Diagram diagram, JavaGenFile java) {
        java.setPackageAndClass(diagram.extensionFactoryClassName)
        executableExtensionFactory.generate(diagram, java)
    }
    
    def generateModelService(Diagram diagram, JavaGenFile java) {
        java.setPackageAndClass(diagram.modelServiceClassName)
        modelService.generate(diagram, java)
    }
    
    def generateGuiceModule(Diagram diagram, JavaGenFile java) {
        java.hasExtensionPoint = true
        java.setPackageAndClass(diagram.guiceModuleClassName)
        guiceModule.generate(diagram, java)
    }
    
    def generateDiagramTypeProvider(Diagram diagram, JavaGenFile java, DiagramTypeProvider diagramTypeProvider) {
        java.setPackageAndClass(diagram.diagramTypeProviderClassName)
        diagramTypeProvider.generate(diagram, java)
    }
    
    def generateFeatureProvider(Diagram diagram, JavaGenFile java, FeatureProvider featureProvider) {
        java.setPackageAndClass(diagram.featureProviderClassName)
        featureProvider.generate(diagram, java)
    }
    
    def generateAddShapeFeatures(Diagram diagram, JavaGenFile java, AddShapeFeature asf) {
        for( node : diagram.nodes) {
            var container = node.shape // as Shape
            java.setPackageAndClass(node.addFeatureClassName)
            var style = container.shape?.dslShape?.style
            if(style != null) {
            	asf.setAttributes(node, style)
            } else {
            	asf.setAttributes(node, diagram.style?.dslStyle)
            }
            asf.generate(container, java)
        }
    }    

    def generateAddConnectionFeatures(Diagram diagram, JavaGenFile java, AddConnectionFeature acf) {
        for( edge : diagram.edges) {
            if (edge.connection.connection == null) {
            	java.setPackageAndClass(edge.addFeatureClassName)
            	acf.generate(edge, java)
            } else {
            	java.setPackageAndClass(edge.addFeatureClassName)
            	addConnectionFromDslFeature.setAttributes(edge, diagram.style)
            	addConnectionFromDslFeature.generate(edge, java)
            }            
        }
    }
    
//    def generateAddReferenceAsConnectionFeature(Diagram diagram, JavaGenFile java, AddReferenceAsConnectionFeature aracf) {
//        for( metaClass : diagram.edges+diagram.nodes) {
//            for( reference : metaClass.references.filter(ref|ref.representedBy != null) ){
//                java.setPackageAndClass(reference.addReferenceAsConnectionFeatureClassName)
//                aracf.setAttributes(diagram.style)
//                aracf.generate(reference, java)
//            }
//        }
//    }    
        
    def generateCreateConnectionFeature(Diagram diagram, JavaGenFile java, CreateConnectionFeature ccf) {
        for( metaClass : diagram.getElementsForTemplate(createConnectionFeature)) {
            java.setPackageAndClass(metaClass.createFeatureClassName)
            ccf.generate(metaClass, java)
        }
    }
    
    def generateCreateShapeFeature(Diagram diagram, JavaGenFile java, CreateShapeFeature csf) {
        for( metaClass : diagram.getElementsForTemplate(createShapeFeature)) {
            java.setPackageAndClass(metaClass.createFeatureClassName)
            csf.generate(metaClass, java)
        }
    }
        
//    def generateCreateReferenceAsConnectionFeature(Diagram diagram, JavaGenFile java, CreateReferenceAsConnectionFeature cracf) {
//        for( metaClass : diagram.metaClasses) {
//            for( reference : metaClass.references.filter(ref|ref.representedBy != null) ) {
//                java.setPackageAndClass(reference.getCreateReferenceAsConnectionFeatureClassName)
//                cracf.generate(reference, java)
//            }
//         }
//    }
    
    def generateUpdateAndLayoutFeatures(Diagram diagram, JavaGenFile java, UpdateConnectionFeature ucf) {
        for( edge : diagram.edges ) {
            if (edge.connection.connection == null) {
                generateUpdateConnectionFeature(edge, edge.connection, java, ucf)
            }
        }    
    }

    def generateUpdateAndLayoutFromDslFeatures(Diagram diagram, JavaGenFile java, UpdateShapeFeature usf, LayoutFromDslFeature lf) {
        for (node : diagram.nodes) {
        	val container = node.shape
            generateLayoutFromDslFeature(node, container, java, lf)
            generateUpdateShapeFromDslFeature(node, container, java, usf)
        }
        
        for( edge : diagram.edges ) {
            if (edge.connection.connection != null) {
                generateUpdateConnectionFromDslFeature(edge, edge.connection, java, updateConnectionFromDslFeature)
            }
        }  
    }

    def generateUpdateShapeFromDslFeature(MetaClass metaClass, Shape container, JavaGenFile java, UpdateShapeFeature usf) {
        java.setPackageAndClass(metaClass.updateFeatureClassName)
        usf.generate(container, java)
    }
    
    def generateUpdateConnectionFeature(MetaClass metaClass, Connection connection, JavaGenFile java, UpdateConnectionFeature ucf) {
        java.setPackageAndClass(metaClass.updateFeatureClassName)
        ucf.generate(connection, java)
    }
    def generateUpdateConnectionFromDslFeature(MetaClass metaClass, Connection connection, JavaGenFile java, UpdateConnectionFromDslFeature ucf) {
        java.setPackageAndClass(metaClass.updateFeatureClassName)
        ucf.generate(connection, java)
    }
    
    def generateLayoutFromDslFeature(MetaClass metaClass, Shape container, JavaGenFile java, LayoutFromDslFeature lf) {
        java.setPackageAndClass(metaClass.layoutFeatureClassName)
        lf.generate(container, java)
    }

//    def generateDeleteReferenceFeature(Diagram diagram, JavaGenFile java, DeleteReferenceFeature drf) {
//        for( metaClass : diagram.metaClasses) {
//            for( reference : metaClass.references ) {
//                java.setPackageAndClass(reference.deleteReferenceFeatureClassName)
//                drf.generate(reference, java)
//            }
//        }
//    }    
    
    def generateImageProvider(Diagram diagram, JavaGenFile java, ImageProvider ip) {
        java.setPackageAndClass(diagram.imageProviderClassName)
        ip.generate(diagram, java)
    }    
    
    def generateToolBehaviourProvider(Diagram diagram, JavaGenFile java, ToolBehaviorProvider tbp) {
        java.setPackageAndClass(diagram.toolBehaviorProviderClassName)
        tbp.generate(diagram, java)
    }    
    
    def generatePropertySection(Diagram diagram, JavaGenFile java, PropertySection ps) {
        for( metaClass : diagram.nodes+diagram.edges) {
            generatePropertySectionForEClassAttributes(diagram, java, metaClass.type, ps)
        }        
    }    

    def generatePropertySectionForEClassAttributes(Diagram diagram, JavaGenFile java, EClass eClass, PropertySection ps) {
        for( attribute : eClass.EAllAttributes){
            generatePropertySectionForAttribute(diagram, java, eClass, attribute, ps)
        }
    }

    def generatePropertySectionForAttribute(Diagram diagram, JavaGenFile java, EClass eClass, EAttribute attribute, PropertySection ps) {
        java.setPackageAndClass(naming.getPropertySectionClassName(eClass, attribute))
        ps.setDiagram(diagram)
        ps.generate(attribute, java)
    }
    
    def generateFilter(Diagram diagram, JavaGenFile java, Filter f, Filter f2) {
        for( metaClass : diagram.nodes+diagram.edges) {
            generateFilter(diagram, java, metaClass, f)
        }
    }    
    
    def generateFilter(Diagram diagram, JavaGenFile java, MetaClass metaClass, Filter f) {
        f.setDiagram(diagram)
        java.setPackageAndClass(metaClass.filterClassName)
        f.generate(metaClass.type, java)
    }    
    
    def generateFilter(Diagram diagram, JavaGenFile java, EClass eClass, Filter f) {
        f.setDiagram(diagram)
        java.setPackageAndClass(eClass.filterClassName)
        f.generate(eClass, java)
    }    
    
    // FIXME CustomBehavior -> Action ???
    def generateCustomFeature(Diagram diagram, JavaGenFile java, CustomFeature cf) {
        for(Action behavior : diagram.getBehaviorsForTemplate(cf)) {
            java.setPackageAndClass(behavior.customFeatureClassName)
            cf.generate(behavior, java)
        }
    }
    
    def generateDirectEditFeature(Diagram diagram, JavaGenFile java, DirectEditFeature df) {
        for(metaClass : diagram.nodes+diagram.edges) {
            java.setPackageAndClass(metaClass.directEditFeatureClassName)
            df.generate(metaClass, java)
        }
    }
    
    def generateCopyFeature(Diagram diagram, JavaGenFile java, CopyFeature cf) {
            java.setPackageAndClass(diagram.copyFeatureClassName)
            cf.generate(diagram, java)
    }
    
    def generatePasteFeature(Diagram diagram, JavaGenFile java, PasteFeature pf) {
            java.setPackageAndClass(diagram.pasteFeatureClassName)
            pf.generate(diagram, java)
        
    }
    
    def generateResizeFeature(Diagram diagram, JavaGenFile java, ResizeFeature lf) {
        for(node : diagram.nodes) {
            java.setPackageAndClass(node.resizeFeatureClassName)
            lf.generate(node.shape, java)
        }   
    }

    def generateMoveFeature(Diagram diagram, JavaGenFile java, MoveFeature lf) {
        for(node : diagram.nodes) {
            java.setPackageAndClass(node.moveFeatureClassName)
            lf.generate(node.shape, java)
        }   
    }
}
