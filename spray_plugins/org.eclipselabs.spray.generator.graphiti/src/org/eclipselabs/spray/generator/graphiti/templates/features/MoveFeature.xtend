/** ****************************************************************************
 * Copyright (c)  The Spray Project.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Spray Dev Team - initial API and implementation
 **************************************************************************** */
package org.eclipselabs.spray.generator.graphiti.templates.features

import javax.inject.Inject
import org.eclipselabs.spray.common.generator.ProjectProperties
import org.eclipselabs.spray.generator.graphiti.util.NamingExtensions
import org.eclipselabs.spray.Shape
import org.eclipselabs.spray.xtext.generator.FileGenerator

import static org.eclipselabs.spray.common.generator.GeneratorUtil.*
import org.eclipselabs.spray.ShapeCompartment

class MoveFeature extends FileGenerator<Shape>{

    @Inject extension NamingExtensions

    override CharSequence generateBaseFile(Shape modelElement) {
        mainFile( modelElement, javaGenFile.baseClassName)
    }

    override CharSequence generateExtensionFile(Shape modelElement) {
        mainExtensionPointFile( modelElement, javaGenFile.className)
    }

    def mainExtensionPointFile(Shape container, String className) '''
        «extensionHeader(this)»
        package «feature_package()»;

        import org.eclipse.graphiti.features.IFeatureProvider;

        public class «className» extends «className»Base {
            public «className»(final IFeatureProvider fp) {
                super(fp);
            }
        }
    '''

    def mainFile(Shape container, String className) '''
		«header(this)»
		package «feature_package()»;

		import org.eclipse.graphiti.features.IFeatureProvider;
		import org.eclipse.graphiti.features.IRemoveFeature;
		import org.eclipse.graphiti.features.context.IMoveShapeContext;
		import org.eclipse.graphiti.features.impl.DefaultMoveShapeFeature;
		import org.eclipse.graphiti.features.context.impl.AddContext;
		import org.eclipse.graphiti.features.context.impl.RemoveContext;
		
		import org.eclipse.graphiti.mm.pictograms.Anchor;
		import org.eclipse.graphiti.mm.pictograms.AnchorContainer;
		import org.eclipse.graphiti.mm.pictograms.Connection;
		import org.eclipse.graphiti.mm.pictograms.Diagram;
		import org.eclipse.graphiti.mm.pictograms.PictogramElement;
		import org.eclipse.graphiti.mm.pictograms.ContainerShape;
		import org.eclipse.graphiti.mm.pictograms.Shape;
		
		import org.eclipselabs.spray.runtime.graphiti.GraphitiProperties;
		import org.eclipselabs.spray.runtime.graphiti.ISprayConstants;
		import org.eclipselabs.spray.runtime.graphiti.layout.SprayLayoutService;
		import org.eclipselabs.spray.runtime.graphiti.layout.ISprayLayoutManager;
		import org.eclipselabs.spray.runtime.graphiti.layout.SprayFitLayoutManager;
		import org.eclipselabs.spray.runtime.graphiti.layout.SprayFixedLayoutManager;
		import org.eclipselabs.spray.runtime.graphiti.layout.SprayTopLayoutManager;
		import org.eclipselabs.spray.runtime.graphiti.layout.SprayDiagramLayoutManager;
		import org.eclipselabs.spray.runtime.graphiti.shape.SprayLayoutManager;
		import org.eclipselabs.spray.runtime.graphiti.rendering.ConnectionRendering;
		
		import «ProjectProperties::shapesPackage».«container.shape.simpleName»Shape;

		// MARKER_IMPORT

		public abstract class «className» extends DefaultMoveShapeFeature {

			SprayLayoutManager layoutManager; 
			«generate_additionalFields(container)»

			public «className»(final IFeatureProvider fp) {
				super(fp);
				layoutManager =  new «container.shape.simpleName»Shape(fp).getShapeLayout( );
			}

			«generateCanMoveShape(container, className, container.node.referencesTo)»
			«generateMoveShape(container, className, container.node.referencesTo)»
			«generate_additionalMethods(container)»
		}
    '''
    
    def generateCanMoveShape(Shape container, String className, Iterable<ShapeCompartment> compartments)'''
		«overrideHeader»
		public boolean canMoveShape(IMoveShapeContext context) {
			Shape sourceShape = (Shape) context.getPictogramElement();
			ContainerShape targetContainer = context.getTargetContainer();
			Object source = getBusinessObjectForPictogramElement(sourceShape );
			Object target = getBusinessObjectForPictogramElement(targetContainer);
			// check whether it can move in the same container
			if( sourceShape.eContainer() == targetContainer ){
				if ((SprayLayoutService.getLayoutManager(sourceShape.getContainer()) instanceof SprayFixedLayoutManager)) {
					return true;
				}
				if ((SprayLayoutService.getLayoutManager(sourceShape.getContainer()) instanceof SprayTopLayoutManager)) {
					return true;
				}
				if ((SprayLayoutService.getLayoutManager(sourceShape.getContainer()) instanceof SprayDiagramLayoutManager)) {
					return true;
				}
				if ((SprayLayoutService.getLayoutManager(sourceShape.getContainer()) instanceof SprayFitLayoutManager)) {
					return true;
				}
				return false;
			}
			«FOR ref : compartments.filter(ref | ! ref.nestedShape.containment)»
				// Cannot drag from a location where the object is a non containment reference
				if ( getBusinessObjectForPictogramElement(sourceShape.getContainer())instanceof «ref.shape.node.itfName») {
					return false;
				}
			«ENDFOR»
			«FOR ref : compartments.filter(ref | ! ref.nestedShape.containment)»
				// target is non containment, and does not contain reference to moved object 
				if (target instanceof «ref.shape.node.itfName») {
					if (SprayLayoutService.isCompartment(context.getTargetContainer())) {
						String id = GraphitiProperties.get(context.getTargetContainer(), ISprayConstants.TEXT_ID);
						if ((id != null) && (id.equals("«ref.compartment.simpleName»"))) {
							if( ! ((«ref.shape.node.itfName»)target).get«ref.nestedShape.name.toFirstUpper»().contains(source) ){
								return true;
							}
						}
					}
				}
			«ENDFOR»
			«FOR ref : compartments.filter(ref | ref.nestedShape.containment)»
				// Can move from containment to another containment compartment
				if (target instanceof «ref.shape.node.itfName») {
					if (SprayLayoutService.isCompartment(context.getTargetContainer())) {
						String id = GraphitiProperties.get(context.getTargetContainer(), ISprayConstants.TEXT_ID);
						if ((id != null) && (id.equals("«ref.compartment.simpleName»"))) {
							return true;
						}
					}
				}
			«ENDFOR»
			return super.canMoveShape(context);
		}
	'''
    
    def generateMoveShape(Shape container, String className, Iterable<ShapeCompartment> compartments)'''
		«overrideHeader»
		public void moveShape(IMoveShapeContext context) {
			PictogramElement sourceShape = context.getPictogramElement();
			ContainerShape targetContainer = context.getTargetContainer();
			ContainerShape sourceContainer = context.getSourceContainer();
			Object sourceParent = getBusinessObjectForPictogramElement(sourceContainer);
			Object source = getBusinessObjectForPictogramElement(sourceShape);
			Object target = getBusinessObjectForPictogramElement(targetContainer);
			if( sourceShape.eContainer() == targetContainer ){ 
				super.moveShape(context);
				final Diagram diagram = this.getDiagram();
				ISprayLayoutManager mgr = SprayLayoutService.getLayoutManager(diagram);
				// Need to layout twice, probably because this is a fit within a fit layout
				// Only neccesary when contents is moved to xcoordinates < 0
				mgr.layout();
				mgr.layout();
				
				// Render all connections of the moved element (if necessary)
				if(sourceShape instanceof AnchorContainer) {
					AnchorContainer anchorContainer = (AnchorContainer) sourceShape;
					for(Anchor a : anchorContainer.getAnchors()) {
						for(Connection incoming : a.getIncomingConnections()) {
							ConnectionRendering.startRendering(incoming.getStart(), incoming.getEnd());
						}
						for(Connection outgoing : a.getOutgoingConnections()) {
							ConnectionRendering.startRendering(outgoing.getStart(), outgoing.getEnd());
						}
					}
				}
				
				return;   
			}
			«FOR c : compartments.filter(ref | ! ref.nestedShape.containment)»
				if (target instanceof «c.shape.node.itfName») {
					if (SprayLayoutService.isCompartment(context.getTargetContainer())) {
						String id = GraphitiProperties.get(context.getTargetContainer(), ISprayConstants.TEXT_ID);
						if ((id != null) && (id.equals("«c.compartment.simpleName»"))) {
							«IF c.nestedShape.many»
							((«c.shape.node.itfName») target).get«c.nestedShape.name.toFirstUpper»().add((«container.node.itfName») source);
							«ELSE»
							((«c.shape.node.itfName») target).set«c.nestedShape.name.toFirstUpper»((«container.node.itfName») source);
							«ENDIF»
							AddContext addContext = new AddContext();
							addContext.putProperty(ISprayConstants.PROPERTY_ALIAS, GraphitiProperties.get(sourceShape, ISprayConstants.PROPERTY_ALIAS));
							addContext.setNewObject(source);
							addContext.setLocation(context.getX(), context.getX());
							addContext.setTargetContainer(targetContainer);
							getFeatureProvider().addIfPossible(addContext);
							return;
						}
					}
				}
			«ENDFOR»
			«FOR c : compartments.filter(ref | ref.nestedShape.containment)»
			if (target instanceof «c.shape.node.itfName») {  // For shape «container.node.name» + «container.shape.dslShape.name»
				if (SprayLayoutService.isCompartment(targetContainer) ) {
					String id = GraphitiProperties.get(targetContainer, ISprayConstants.TEXT_ID);
					if ((id != null) && (id.equals("«c.compartment.simpleName»"))) {

						sourceContainer.getChildren().remove(source);
						ContainerShape sourceTop = SprayLayoutService.findTopDslShape(sourceContainer);
						if( sourceTop != null ){
							SprayLayoutService.getLayoutManager(sourceTop).layout();
						}
						// remove from source container and add to target container

						«IF c.nestedShape.many»
						((«c.shape.node.itfName») target).get«c.nestedShape.name.toFirstUpper»().add((«container.node.itfName») source);
						«ELSE»
						((«c.shape.node.itfName») target).set«c.nestedShape.name.toFirstUpper»((«container.node.itfName») source);
						«ENDIF»
						sourceShape.getGraphicsAlgorithm().setX(context.getX());
						sourceShape.getGraphicsAlgorithm().setY(context.getY());
						targetContainer.getChildren().add((Shape)sourceShape);
						ContainerShape targetTop = SprayLayoutService.findTopDslShape(targetContainer);
						if (targetTop != null) {
							SprayLayoutService.getLayoutManager(targetTop).layout();
						}
						return;
					}
				}
			}
			«ENDFOR»
		}
     '''
}