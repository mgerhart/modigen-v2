/** ****************************************************************************
 * Copyright (c)  The Spray Project.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Spray Dev Team - initial API and implementation
 **************************************************************************** */
package org.eclipselabs.spray.generator.graphiti.util.mm

/**
 * Extension methods for type MetaClass
 */
import org.eclipselabs.spray.MetaClass
import javax.inject.Inject
import org.eclipselabs.spray.generator.graphiti.util.NamingExtensions
import org.eclipselabs.spray.Action
import java.util.Set
import java.util.TreeSet

import org.eclipselabs.spray.Edge
import java.util.Map
import java.util.ArrayList
import java.util.HashMap

class MetaClassExtensions {
    @Inject extension NamingExtensions 
        
    def getCreateFeatureDescription (MetaClass metaClass) {
        "Create new "+metaClass.name
    }

    /**
     * @return <code>true</code> when the MetaClass is represented by a Connection Shape
     */
    def boolean isRepresentedByConnection (MetaClass mc) {
        mc instanceof Edge
    }
    
    /**
     * Returns the ICustomFeature instances for a MetaClass
     */
    def Set<String> getCustomFeatureClassNames (MetaClass clazz) {
        val result = new TreeSet<String>()
        for (action: getAllActions(clazz)) {
            result += action.customFeatureClassName.shortName
        }
        result
    }
    
    /**
     * Combines the direct Actions with those from the referred ActionGroups. 
     */
    def Iterable<Action> getAllActions(MetaClass clazz) {
        
        if (clazz.actionGroup != null) {
        	var Iterable<Action> globalActions = clazz.actionGroup.actionIncludes.map(a|a.globalActionIncludes.map(i|i.actions).flatten).flatten
	        var Iterable<Action> localActions = clazz.actionGroup.actions
	        
	        var Map<String, Action> actionMap = new HashMap<String, Action>()
	        
	        for (action : globalActions) {
	        	actionMap.put(action.label, action)
	        }
	        
	        for (action : localActions) {
	        	actionMap.put(action.label, action)
	        }
	        
	        actionMap.values
        } else {
        	new ArrayList<Action>()
        }
          
    }
    
    def boolean hasOnCreate (MetaClass clazz) {
        (clazz.onCreate != null)
    }
    
    def boolean hasContainer (MetaClass clazz) {
    	(clazz.container != null)
    }
}