/** ****************************************************************************
 * Copyright (c)  The Spray Project.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Spray Dev Team - initial API and implementation
 **************************************************************************** */
package org.eclipselabs.spray.generator.graphiti.util

import javax.inject.Inject
import org.eclipse.emf.ecore.EAttribute
import org.eclipse.emf.ecore.EClass
import org.eclipse.emf.ecore.EClassifier
import org.eclipse.emf.ecore.EDataType
import org.eclipse.emf.ecore.EObject
import org.eclipse.emf.ecore.EReference
import org.eclipse.emf.ecore.EcorePackage
import org.eclipse.xtext.common.types.JvmTypeReference
import org.eclipse.xtext.util.SimpleAttributeResolver
import org.eclipselabs.spray.Action
import org.eclipselabs.spray.Diagram
import org.eclipselabs.spray.MetaClass
import org.eclipselabs.spray.Shape
import org.eclipselabs.spray.xtext.generator.importmanager.ImportUtil
import org.eclipselabs.spray.xtext.util.GenModelHelper

import static org.eclipse.xtext.EcoreUtil2.*
import static org.eclipselabs.spray.common.generator.GeneratorUtil.*
import org.eclipselabs.spray.ShapeReference
import org.eclipselabs.spray.ConnectionReference
import org.eclipselabs.spray.ShapeDslKey
import org.eclipselabs.spray.SprayStyleRef
import org.eclipselabs.spray.common.generator.ProjectProperties
import org.eclipselabs.spray.styles.Style
import org.eclipselabs.spray.shapes.ShapeStyleRef
import org.eclipselabs.spray.Node
import org.eclipselabs.spray.Edge
import org.eclipselabs.spray.ShapeCompartment

/**
 * Computation of class names, file names etc.
 * @author Karsten Thoms
 */
class NamingExtensions {
    @Inject GenModelHelper genModelHelper
    @Inject ImportUtil importUtil

//    def dispatch String qualifiedName(ShapeDslKey shapeRef) {
//        if( shapeRef.dslKey!= null){
//            return "org.eclipselabs.spray.shapes." + shapeRef.dslShape.name.toFirstUpper
//        } else {
//            return shapeRef.jvmShape.qualifiedName;
//        }   
//    }
    
    def dispatch String simpleName(ShapeDslKey shapeRef) {
        if( shapeRef.dslKey != null){
            return shapeRef.dslKey
        } else {
            return shapeRef.jvmKey.simpleName;
        }   
    }
    
    def dispatch String qualifiedName(ShapeReference shapeRef) {
        if( shapeRef.dslShape != null){
            return ProjectProperties::shapesPackage + "." + shapeRef.dslShape.name.toFirstUpper
        } else {
            return shapeRef.jvmShape.qualifiedName;
        }   
    }
    
    def dispatch String simpleName(ShapeReference shapeRef) {
        if( shapeRef.dslShape != null){
            return shapeRef.dslShape.name.toFirstUpper
        } else {
            return shapeRef.jvmShape.simpleName;
        }   
    }
    
    def dispatch String qualifiedName(ConnectionReference shapeRef) {
        if( shapeRef.dslConnection != null){
            return ProjectProperties::shapesPackage + "." + shapeRef.dslConnection.name.toFirstUpper
        } else {
            return shapeRef.jvmConnection.qualifiedName;
        }   
    }
    
    def dispatch String simpleName(ConnectionReference shapeRef) {
        if( shapeRef.dslConnection != null){
            return shapeRef.dslConnection.name.toFirstUpper
        } else {
            return shapeRef.jvmConnection.simpleName;
        }   
    }

    def dispatch String qualifiedName(SprayStyleRef styleRef) {
        if( styleRef.javaStyle != null){
            return styleRef.javaStyle.qualifiedName;
        } else {
            return styleRef.dslStyle.qualifiedName
        }   
    }
    
    def dispatch String qualifiedName(ShapeStyleRef styleRef) {
        if( styleRef.javaStyle != null){
            return styleRef.javaStyle.qualifiedName;
        } else {
            return styleRef.dslStyle.qualifiedName
        }   
    }
    
    def dispatch String qualifiedName(Style style) {
        return ProjectProperties::stylesPackage + "." + style.name.toFirstUpper
    }
    
    def dispatch String simpleName(SprayStyleRef styleRef) {
        if( styleRef.javaStyle != null){
            return styleRef.javaStyle.simpleName;
        } else {
            return styleRef.dslStyle.name.toFirstUpper
        }   
    }
    
    def dispatch String getName (EObject obj) {
        SimpleAttributeResolver::NAME_RESOLVER.apply(obj)
    }

    def dispatch String getName (MetaClass metaClass) {
        if (metaClass.type!=null) metaClass.type.name else null
    }

    //---------------------------------------------------------------------------------------------
    // Class names for Diagram
    //---------------------------------------------------------------------------------------------
    def getDiagramTypeProviderClassName (Diagram diagram) {
        diagram_package() + "." + diagram.getDiagramTypeProviderSimpleClassName
    }
    def getDiagramTypeProviderSimpleClassName (Diagram diagram) {
        diagram.name.toFirstUpper + "DiagramTypeProvider"
    }
    def getFeatureProviderClassName (Diagram diagram) {
        diagram_package() + "." + diagram.getFeatureProviderSimpleClassName
    }
    def getFeatureProviderSimpleClassName (Diagram diagram) {
        diagram.name.toFirstUpper + "FeatureProvider"
    }
    def getImageProviderClassName (Diagram diagram) {
        diagram_package() + "." + diagram.getImageProviderSimpleClassName
    }
    def getImageProviderSimpleClassName (Diagram diagram) {
        diagram.name.toFirstUpper + "ImageProvider"
    }
    def getToolBehaviorProviderClassName (Diagram diagram) {
        diagram_package() + "." + diagram.getToolBehaviorProviderSimpleClassName
    }
    def getToolBehaviorProviderSimpleClassName (Diagram diagram) {
        diagram.name.toFirstUpper + "ToolBehaviorProvider"
    }
    def getDiagramEditorClassName (Diagram diagram) {
        diagram_package() + "." + diagram.getDiagramEditorSimpleClassName
    }
    def getDiagramEditorSimpleClassName (Diagram diagram) {
        diagram.name.toFirstUpper() + "DiagramEditor"
    }
    def getGuiceModuleClassName (Diagram diagram) {
        main_package() + "." + getGuiceModuleSimpleClassName(diagram)
    }
    def getGuiceModuleSimpleClassName (Diagram diagram) {
        diagram.name.toFirstUpper+"Module"
    }
    def getModelServiceClassName (Diagram diagram) {
        diagram_package + "." + diagram.getModelServiceSimpleClassName
    }
    def getModelServiceSimpleClassName (Diagram diagram) {
        diagram.name.toFirstUpper() + "ModelService"
    }
    //---------------------------------------------------------------------------------------------
    // Class names for MetaClass
    //---------------------------------------------------------------------------------------------
    def getCreateFeatureClassName (MetaClass clazz) {
        getFeatureClassName (clazz, FeatureType::Create)
    }
    def getCreateFeatureSimpleClassName (MetaClass clazz) {
        getFeatureSimpleClassName (clazz, FeatureType::Create)
    }
    def getAddFeatureClassName (MetaClass clazz) {
        getFeatureClassName (clazz, FeatureType::Add)
    }
    def getAddFeatureSimpleClassName (MetaClass clazz) {
        getFeatureSimpleClassName (clazz, FeatureType::Add)
    }
    def getUpdateFeatureClassName (MetaClass clazz) {
        getFeatureClassName (clazz, FeatureType::Update)
    }
    def getUpdateFeatureSimpleClassName (MetaClass clazz) {
        getFeatureSimpleClassName (clazz, FeatureType::Update)
    }
    def getLayoutFeatureClassName (MetaClass clazz) {
        getFeatureClassName (clazz, FeatureType::Layout)
    }
    def getLayoutFeatureSimpleClassName (MetaClass clazz) {
        getFeatureSimpleClassName (clazz, FeatureType::Layout)
    }
    def getDirectEditFeatureClassName (MetaClass clazz) {
        getFeatureClassName (clazz, FeatureType::DirectEdit)
    }
    def getDirectEditFeatureSimpleClassName (MetaClass clazz) {
        getFeatureSimpleClassName (clazz, FeatureType::DirectEdit)
    }
    
    def getCopyFeatureClassName (Diagram clazz) {
   		getDiagramClassName(clazz, FeatureType::Copy)
    }   
    
    def getCopyFeatureSimpleClassName (Diagram clazz) {
    	getDiagramSimpleClassName(clazz, FeatureType::Copy)
    }
    
    def getPasteFeatureClassName (Diagram clazz) {
    	getDiagramClassName(clazz, FeatureType::Paste)
    }
        
    def getPasteFeatureSimpleClassName (Diagram clazz) {
		getDiagramSimpleClassName(clazz, FeatureType::Paste)
    }
    
    def getResizeFeatureClassName (MetaClass clazz) {
        getFeatureClassName (clazz, FeatureType::Resize)
    }
    def getResizeFeatureSimpleClassName (MetaClass clazz) {
        getFeatureSimpleClassName (clazz, FeatureType::Resize)
    }
    def getMoveFeatureClassName (MetaClass clazz) {
        getFeatureClassName (clazz, FeatureType::Move)
    }
    def getMoveFeatureSimpleClassName (MetaClass clazz) {
        getFeatureSimpleClassName (clazz, FeatureType::Move)
    }

    /**
     * Result example:
     * Diagram = "mod4j", MetaClass = "BusinessClass", featureType="Add" 
     * Result = "Mod4jAddBusinessClassFeature"
     */
    def getFeatureClassName (MetaClass clazz, FeatureType featureType) {
        feature_package() + "." + getFeatureSimpleClassName(clazz, featureType)
    }

    def getFeatureSimpleClassName (MetaClass clazz, FeatureType featureType) {
        getDiagramFromMetaClass(clazz).name.toFirstUpper + featureType + clazz.name + "Feature"
    }
    
    def getDiagramClassName (Diagram clazz, FeatureType featureType) {
    	feature_package() + "." + getDiagramSimpleClassName(clazz, featureType)
    }
    
    def getDiagramSimpleClassName (Diagram clazz, FeatureType featureType) {
    	clazz.name.toFirstUpper + featureType + "Feature"
    }
    
    def dispatch getFilterClassName (MetaClass clazz) {
        property_package() + "." + clazz.getFilterSimpleClassName
    }
    def dispatch getFilterSimpleClassName (MetaClass clazz) {
        clazz.type.name+"Filter"
    }

    
    //---------------------------------------------------------------------------------------------
    // Class names for Behaviour
    //---------------------------------------------------------------------------------------------
    /**
     * Returns the qualified class name of the ICustomFeature represented by a CustomBehavior.
     */
    def getCustomFeatureClassName (Action behavior) {
        if (behavior.realizedBy == null)
            (feature_package() + "." + behavior.getCustomFeatureSimpleClassName)
        else
            behavior.realizedBy.type.qualifiedName
    }
    def getCustomFeatureSimpleClassName (Action behavior) {
        val diagram = getContainerOfType(behavior, typeof(Diagram))
        diagram.name.toFirstUpper + "Custom" + behavior.name.toFirstUpper  + "Feature"
    }
    
    //---------------------------------------------------------------------------------------------
    // Class names for EClass
    //---------------------------------------------------------------------------------------------
    def dispatch getFilterClassName (EClass clazz) {
        property_package() + "." + clazz.getFilterSimpleClassName
    }
    def dispatch getFilterSimpleClassName (EClass clazz) {
        clazz.name+"Filter"
    }
    //---------------------------------------------------------------------------------------------
    // Class names for EAttribute
    //---------------------------------------------------------------------------------------------
    def getPropertySectionClassName (EAttribute attribute) {
        property_package() + "." + attribute.getPropertySectionSimpleClassName
    }
    def getPropertySectionSimpleClassName (EAttribute attribute) {
        attribute.EContainingClass.name + attribute.name.toFirstUpper + "Section"
    }
    def getPropertySectionClassName (EClass clazz, EAttribute attribute) {
        property_package() + "." + getPropertySectionSimpleClassName(clazz,attribute)
    }
    def getPropertySectionSimpleClassName (EClass clazz, EAttribute attribute) {
        clazz.name + attribute.name.toFirstUpper + "Section"
    }
    //---------------------------------------------------------------------------------------------
    // Names from GenModel
    //---------------------------------------------------------------------------------------------
    /**
     * Computes the qualified Interface name of the EClass and shortens it using the shortName() function.
     */
    def String getItfName (MetaClass clazz) {
        this.getJavaInterfaceName(clazz.type)
    }
    /**
     * Computes the qualified Interface name of the EClass and shortens it using the shortName() function.
     */
    def String getItfName (EClass clazz) {
        this.getJavaInterfaceName(clazz)
    }

    def String getJavaInterfaceName (MetaClass clazz) {
        this.getJavaInterfaceName(clazz.type)
    }
    def String getJavaInterfaceName (EClass eClass) {
        genModelHelper.getJavaInterfaceName(eClass)
    }
    def String getJavaInterfaceName (EClassifier eClassifier) {
        genModelHelper.getJavaInterfaceName(eClassifier)
    }
    def String getEPackageClassName (MetaClass clazz) {
        this.getEPackageClassName(clazz.type)
    }
    def String getEPackageClassName (EClassifier eClassifier) {
        // remove the need to register Ecore Genmodel in a standalone workflow
        // although this usually is not a problem, we had some on CI for the examples project
        if (eClassifier.EPackage==EcorePackage::eINSTANCE)
            "org.eclipse.emf.ecore.EcorePackage"
        else
            genModelHelper.getEPackageClassifierName(eClassifier)
    }
    def String getEFactoryInterfaceName (EClass eClass) {
        if (eClass.EPackage==EcorePackage::eINSTANCE)
            "org.eclipse.emf.ecore.EcoreFactory"
        else
            genModelHelper.getEFactoryInterfaceName(eClass)
    }
    def String getEFactoryInterfaceName (MetaClass clazz) {
        this.getEFactoryInterfaceName(clazz.type)
    }
    def String getEFactoryInterfaceName (EDataType dataType) {
        if (dataType.EPackage==EcorePackage::eINSTANCE)
            "org.eclipse.emf.ecore.EcoreFactory"
        else
            genModelHelper.getEFactoryInterfaceName(dataType)
    }
    def String getLiteralConstant (EClass eClass) {
        genModelHelper.getEPackageClassName(eClass).shortName+".Literals."+genModelHelper.getLiteralConstant(eClass)
    }

    def String getLiteralConstant (EReference eReference) {
        genModelHelper.getEPackageClassName(eReference.EContainingClass).shortName+".Literals."+genModelHelper.getLiteralConstant(eReference)
    }
    def String getLiteralConstant (MetaClass clazz) {
        getLiteralConstant(clazz.type)
    }
    //---------------------------------------------------------------------------------------------
    // Other names for MetaClass
    //---------------------------------------------------------------------------------------------
    def String getDiagramName (MetaClass clazz) {
        getDiagramFromMetaClass(clazz).name 
    }
    //---------------------------------------------------------------------------------------------
    // Other names
    //---------------------------------------------------------------------------------------------
    def getActivatorClassName (Diagram diagram) {
        main_package() + "." + getActivatorSimpleClassName(diagram)
    }
    def getActivatorSimpleClassName (Diagram diagram) {
        "Activator"
    }
    def getExtensionFactoryClassName (Diagram diagram) {
        main_package() + ".internal." + getExtensionFactorySimpleClassName(diagram)
    }
    def getExtensionFactorySimpleClassName (Diagram diagram) {
        "ExecutableExtensionFactory"
    }
    def String getImageIdentifier (Diagram diagram, String imagePath) {
        if (imagePath==null) return null
        // strip file extension and replace all non-word characters by underscore
        return diagram.name.toUpperCase+"__"+imagePath.substring(0, imagePath.lastIndexOf('.')).replaceAll("\\W","_").toUpperCase
    }
    def String getImageBaseName (String imagePath) {
        if (imagePath==null) return null
        // strip file extension and replace all non-word characters by underscore
        return imagePath.substring(0, imagePath.lastIndexOf('.')).replaceAll("\\W","_").toLowerCase
    }
    
    def String getModelFileExtension (EObject ctx) {
        return ProjectProperties::getModelFileExtension();
    }
    def String shortName (JvmTypeReference typeRef) {
        return importUtil.shortName(typeRef)
    }
    def String shortName (String qualifiedName) {
        return importUtil.shortName(qualifiedName)
    }
    def String shortName (Class<?> clazz) {
        return importUtil.shortName(clazz)
    }
    
    def Iterable<ShapeCompartment> getReferencesTo (Node node) {
        var referringShapes = node.diagram.nodes.map(n | n.shape)
        var compartmentAssignments = referringShapes.map(s | s.shapeCompartments).flatten   				
        var result = compartmentAssignments.filter(ca | (node.type == ca.nestedShape.EReferenceType) || node.type.EAllSuperTypes.contains(ca.nestedShape.EReferenceType))
        result
    }

    def Iterable<Shape> xgetReferringShapes (MetaClass clazz) {
    	getDiagramFromMetaClass(clazz).nodes.map(n|n.shape)
    }
    
    //---------------------------------------------------------------------------------------------
    // Helper methods
    //---------------------------------------------------------------------------------------------
    
    def getDiagramFromMetaClass (MetaClass clazz) {
    	if (clazz instanceof Node) {
    		var Node node = clazz as Node
    		node.diagram
    	} else {
    		var Edge edge = clazz as Edge
    		edge.diagram
    	}
    }
}

